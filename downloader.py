import telebot
import config
import os

bot = telebot.TeleBot(config.token)

@bot.message_handler(commands=['download'])
def find_file_ids(message):
    with open("output_photo.txt", 'w+') as file_handler:
        for file in os.listdir('photo/events/'):
            f = open('photo/events/'+file, 'rb')
            msg = bot.send_photo(message.chat.id, f)
            # bot.send_message(message.chat.id, msg.voice.file_id, reply_to_message_id=msg.message_id)
            file_handler.write("{}  {}\n".format(file, msg.photo[len(msg.photo)-1].file_id))
            f.close()
    with open('output_photo.txt', 'rb') as file_handler:
        bot.send_document(message.chat.id, file_handler)

if __name__ == '__main__':
    bot.polling(none_stop=True)