import telebot
import os
from telebot.types import *
import time
import requests

import config
import dbworker

bot = telebot.TeleBot(config.debug_token, threaded=False)


def delete_prev_inline(message, add=0):
    try:
        bot.edit_message_reply_markup(message.chat.id, message_id=message.message_id - 1 + add,
                                      reply_markup=InlineKeyboardMarkup())
    except Exception:
        pass


# Начало диалога
@bot.message_handler(commands=["start"])
def cmd_start(message):
    db = dbworker.SQLighter(config.db_file)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')
    delete_prev_inline(message)
    reply_markup = ReplyKeyboardMarkup(resize_keyboard=True, row_width=1)
    reply_markup.add(KeyboardButton(vocab['on_kz_lang']),
                     KeyboardButton(vocab['on_ru_lang']))
    bot.send_message(message.chat.id, vocab['start_message'], reply_markup=reply_markup)
    bot.register_next_step_handler_by_chat_id(message.chat.id, callback=process_lang_step)


def process_lang_step(message):
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')
    if message.text == vocab['on_ru_lang']:
        vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')
        bot.send_message(message.from_user.id, text=vocab['input_name'], reply_markup=ReplyKeyboardRemove(),
                         parse_mode='Markdown')
        bot.register_next_step_handler_by_chat_id(chat_id=message.chat.id, callback=process_name_step_start,
                                                  user_data=["RU"])
    elif message.text == vocab['on_kz_lang']:
        vocab = json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
        bot.send_message(message.from_user.id, text=vocab['input_name'], reply_markup=ReplyKeyboardRemove(),
                         parse_mode='Markdown')
        bot.register_next_step_handler_by_chat_id(chat_id=message.chat.id, callback=process_name_step_start,
                                                  user_data=["KZ"])
    else:
        reply_markup = ReplyKeyboardMarkup(resize_keyboard=True, row_width=1)
        reply_markup.add(KeyboardButton(vocab['on_kz_lang']), KeyboardButton(vocab['on_ru_lang']))
        bot.send_message(message.chat.id, vocab['select_lang'], reply_markup=reply_markup)
        bot.register_next_step_handler_by_chat_id(chat_id=message.chat.id, callback=process_lang_step)


def process_name_step_start(message, user_data):
    lang = user_data[0]
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    name = message.text
    markup = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    markup.add(KeyboardButton(text=vocab['reply_button'], request_contact=True))
    bot.send_message(chat_id=message.chat.id, text=vocab['reply_contact'], reply_markup=markup, parse_mode='Markdown')
    user_data.append(name)
    bot.register_next_step_handler_by_chat_id(chat_id=message.chat.id, callback=finish_registration,
                                              user_data=user_data)


def finish_registration(message, user_data):
    lang = user_data[0]
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')

    if not message.contact:
        markup = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
        markup.add(KeyboardButton(text=vocab['reply_button'], request_contact=True))
        bot.send_message(chat_id=message.chat.id, text=vocab['reply_contact'], reply_markup=markup,
                         parse_mode='Markdown')
        bot.register_next_step_handler_by_chat_id(chat_id=message.chat.id, callback=finish_registration,
                                                  user_data=user_data)
        return
    name = user_data[1]
    number = config.validate_number(message.contact.phone_number)
    db = dbworker.SQLighter(config.db_file)
    db.add_user(user_id=message.chat.id, state=config.States.S_MAIN_MENU.value,
                lang=lang, number=number, name=name)
    send_main_menu(message, vocab)


def send_main_menu(message, vocab):
    reply_markup = ReplyKeyboardMarkup(resize_keyboard=True)
    reply_markup.row(KeyboardButton(vocab['about']),
                     KeyboardButton(vocab['inform']))
    reply_markup.row(KeyboardButton(vocab['suggest']),
                     KeyboardButton(vocab['events']))
    reply_markup.row(KeyboardButton(vocab['organizations']),
                     KeyboardButton(vocab['change_lang']))
    bot.send_message(chat_id=message.from_user.id, text=vocab['select'], reply_markup=reply_markup)


@bot.message_handler(commands=["menu"])
def cmd_menu(message):
    try:
        db = dbworker.SQLighter(config.db_file)
        delete_prev_inline(message)
        vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if db.get_language(
            message.from_user.id) == "RU" else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')

        db.set_state(message.from_user.id, config.States.S_MAIN_MENU.value)
        send_main_menu(message, vocab)
        db.close()
    except:
        db = dbworker.SQLighter(config.db_file)
        vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')
        reply_markup = ReplyKeyboardMarkup(resize_keyboard=True, row_width=1)
        reply_markup = ReplyKeyboardMarkup(resize_keyboard=True, row_width=1)
        reply_markup.add(KeyboardButton(vocab['on_kz_lang']),
                         KeyboardButton(vocab['on_ru_lang']))
        bot.send_message(message.chat.id, vocab['start_message'], reply_markup=reply_markup)
        db.add_user(user_id=message.chat.id, state=config.States.S_START.value)
        db.close()


@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'on_ru_lang']
                         or message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
                             'on_kz_lang'])
def callback_inline(message):
    db = dbworker.SQLighter(config.db_file)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')
    lang = None
    if message.text == vocab['on_ru_lang']:
        vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')
        lang = "RU"
    if message.text == vocab['on_kz_lang']:
        vocab = json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
        lang = "KZ"
    db.set_state(message.from_user.id, config.States.S_MAIN_MENU.value)
    db.set_language(message.from_user.id, lang)
    send_main_menu(message, vocab)
    db.close()


@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'about'] or message.text == json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')['about'])
def callback_about(message):
    db = dbworker.SQLighter(config.db_file)
    delete_prev_inline(message)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if db.get_language(
        message.from_user.id) == "RU" else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    try:
        bot.edit_message_reply_markup(chat_id=message.chat.id, message_id=message.message_id - 2,
                                      reply_markup=InlineKeyboardMarkup())
    except Exception:
        pass
    db.set_state(message.from_user.id, config.States.S_ABOUT_MENU.value)

    reply_markup = ReplyKeyboardMarkup(resize_keyboard=True)
    reply_markup.row_width = 2
    reply_markup.add(KeyboardButton(vocab['mission']),
                     KeyboardButton(vocab['work']),
                     KeyboardButton(vocab['rule']),
                     KeyboardButton(vocab['projects']),
                     KeyboardButton(vocab['news']),
                     KeyboardButton(vocab['contacts']))

    reply_markup.row(KeyboardButton(vocab['back_to_menu']))

    bot.send_message(chat_id=message.from_user.id, text=vocab['select'],
                     reply_markup=reply_markup)
    db.close()


@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'news'] or message.text == json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')['news'])
def news(message):
    db = dbworker.SQLighter(config.db_file)
    delete_prev_inline(message)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if db.get_language(
        message.from_user.id) == "RU" else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')

    reply_markup = InlineKeyboardMarkup()
    reply_markup.add(InlineKeyboardButton(text=vocab['link'], url="https://t.me/jastarenu_channel"))
    bot.send_message(chat_id=message.from_user.id, text=vocab['news_message'], reply_markup=reply_markup)
    db.close()


@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'inform'] or message.text == json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')['inform'])
def callback_inform(message):
    # Проверить язык!!!!
    db = dbworker.SQLighter(config.db_file)
    delete_prev_inline(message)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if db.get_language(
        message.from_user.id) == "RU" else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')

    db.set_state(message.from_user.id, config.States.S_INFORM_MENU.value)

    reply_markup = ReplyKeyboardMarkup(resize_keyboard=True)
    reply_markup.row_width = 2
    reply_markup.add(KeyboardButton(vocab['medical']),
                     KeyboardButton(vocab['stud_house']),
                     KeyboardButton(vocab['library']),
                     KeyboardButton(vocab['sport_clubs']),
                     KeyboardButton(vocab['army']))
    reply_markup.row(KeyboardButton(vocab['back_to_menu']))
    bot.send_message(chat_id=message.from_user.id, text=vocab['select'],
                     reply_markup=reply_markup)
    db.close()


@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'projects'] or message.text == json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')[
                             'projects'])
def project_list(message):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    delete_prev_inline(message)
    reply_markup = InlineKeyboardMarkup()
    reply_markup.row_width = 2
    projects = json.loads(open('projects.json', encoding='utf-16').read(), encoding='utf-16')
    for project in projects:
        reply_markup.add(InlineKeyboardButton(projects[project]["BUTTON"][lang], callback_data='s_proj' + project))

    bot.send_message(chat_id=message.from_user.id, text=vocab['select_project'],
                     reply_markup=reply_markup)
    db.close()


@bot.callback_query_handler(func=lambda call: call.data == 'back_to_projects')
def project_list_callback(call):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(call.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    delete_prev_inline(call.message)
    reply_markup = InlineKeyboardMarkup()
    reply_markup.row_width = 2
    projects = json.loads(open('projects.json', encoding='utf-16').read(), encoding='utf-16')
    for project in projects:
        reply_markup.add(InlineKeyboardButton(projects[project]["BUTTON"][lang], callback_data='s_proj' + project))

    bot.send_message(chat_id=call.from_user.id, text=vocab['select_project'],
                     reply_markup=reply_markup)
    bot.answer_callback_query(call.id)
    db.close()


@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'organizations'] or message.text == json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')[
                             'organizations'])
def organization_list_types(message):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    delete_prev_inline(message)
    reply_markup = ReplyKeyboardMarkup(resize_keyboard=True)
    reply_markup.add(KeyboardButton(vocab['jun_org']))
    reply_markup.add(KeyboardButton(vocab['art_org']))
    reply_markup.add(KeyboardButton(vocab['reg_org']))
    reply_markup.add(KeyboardButton(vocab['back_to_menu']))
    bot.send_message(chat_id=message.from_user.id, text=vocab['select_org_type'],
                     reply_markup=reply_markup)
    db.close()


@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'jun_org'] or message.text == json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')[
                             'jun_org'])
def organization_list(message):
    # Проверить язык!!!!
    db = dbworker.SQLighter(config.db_file)
    delete_prev_inline(message)
    lang = db.get_language(message.from_user.id)
    org_type = 'jun'
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')

    reply_markup = InlineKeyboardMarkup()
    orgs = json.loads(open('organizations.json', encoding='utf-16').read(), encoding='utf-16')
    for org in orgs[org_type]:
        reply_markup.add(
            InlineKeyboardButton(orgs[org_type][org]["BUTTON"][lang], callback_data='s_org_' + org_type + '_' + org))

    bot.send_message(chat_id=message.from_user.id, text=vocab['select_org'],
                     reply_markup=reply_markup)
    db.close()


@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'art_org'] or message.text == json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')[
                             'art_org'])
def organization_list(message):
    # Проверить язык!!!!
    db = dbworker.SQLighter(config.db_file)
    delete_prev_inline(message)
    lang = db.get_language(message.from_user.id)
    org_type = 'art'
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')

    reply_markup = InlineKeyboardMarkup()
    orgs = json.loads(open('organizations.json', encoding='utf-16').read(), encoding='utf-16')
    for org in orgs[org_type]:
        reply_markup.add(
            InlineKeyboardButton(orgs[org_type][org]["BUTTON"][lang], callback_data='s_org_' + org_type + '_' + org))

    bot.send_message(chat_id=message.from_user.id, text=vocab['select_org'],
                     reply_markup=reply_markup)
    db.close()


@bot.callback_query_handler(func=lambda call: call.data[:5] == "s_org")
def show_organization(call):
    # Проверить язык!!!!
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(call.from_user.id)
    org_type = call.data[6:9]
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    orgs = json.loads(open('organizations.json', encoding='utf-16').read(), encoding='utf-16')
    organization = call.data[10:]
    reply_markup = InlineKeyboardMarkup()
    bot.edit_message_text(chat_id=call.from_user.id, message_id=call.message.message_id, parse_mode='HTML',
                          text=orgs[org_type][organization]["TEXT"][lang], reply_markup=reply_markup)
    if orgs[org_type][organization]["FILE"][lang]:
        try:
            bot.send_document(call.message.chat.id, orgs[org_type][organization]["FILE"][lang])
        except FileNotFoundError:
            print('FILE NOT FOUND')
    bot.answer_callback_query(call.id)
    db.close()


@bot.callback_query_handler(func=lambda call: call.data[:6] == "s_proj")
def show_project(call):
    # Проверить язык!!!!
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(call.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')

    projects = json.loads(open('projects.json', encoding='utf-16').read(), encoding='utf-16')
    project = call.data[6:]
    reply = InlineKeyboardMarkup()
    if projects[project]["FILE"][lang]:
        reply.add(InlineKeyboardButton(text=vocab['more'], callback_data='file_' + call.data))
    bot.edit_message_text(chat_id=call.from_user.id, text=projects[project]["TEXT"][lang],
                          message_id=call.message.message_id, reply_markup=reply, parse_mode='Markdown')
    bot.answer_callback_query(call.id)
    db.close()


@bot.callback_query_handler(func=lambda call: call.data[:11] == "file_s_proj")
def send_project_file(call):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(call.from_user.id)
    delete_prev_inline(call.message, add=1)
    projects = json.loads(open('projects.json', encoding='utf-16').read(), encoding='utf-16')
    project = call.data[11:]
    if projects[project]["FILE"][lang]:
        bot.send_document(chat_id=call.from_user.id, data=projects[project]["FILE"][lang])
    bot.answer_callback_query(call.id)
    db.close()


@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'contacts'] or message.text == json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')[
                             'contacts'])
def contacts(message):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    delete_prev_inline(message)
    info = json.loads(open('info.json', encoding='utf-16').read(), encoding='utf-16')
    text = info['contacts'][lang]['address'] + '\n'
    for tel in info['contacts'][lang]['telephones']:
        text += tel + '\n'
    text += info['contacts'][lang]['site_text'] + info['contacts'][lang]['site'] + '\n'
    bot.send_message(chat_id=message.from_user.id, text=text)
    bot.send_location(chat_id=message.from_user.id, latitude=info['contacts'][lang]['latitude'],
                      longitude=info['contacts'][lang]['longitude'])
    db.close()


# -----------------------------------------------------------------------------------------------------------------------

@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'events'] or message.text == json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')[
                             'events'])
def events_button(message):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')

    reply_markup = gen_passed_future_reply_markup(vocab)

    bot.send_message(message.from_user.id, text=vocab['select_event'], reply_markup=reply_markup)


def gen_passed_future_reply_markup(vocab):
    reply_markup = ReplyKeyboardMarkup(one_time_keyboard=False, resize_keyboard=True)
    reply_markup.add(vocab['passed_events'], vocab['future_events'])
    reply_markup.add(vocab['back_to_menu'])

    return reply_markup


@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'passed_events'] or message.text == json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')[
                             'passed_events'])
def passed_events_button(message):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    delete_prev_inline(message)
    events = json.loads(open('events.json', encoding='utf-16').read(), encoding='utf-16')
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')

    reply_markup = InlineKeyboardMarkup()
    counter = 0
    for event in events:
        if events[event]["PASSED"][lang] == 'True':
            counter += 1
            reply_markup.add(
                InlineKeyboardButton(events[event]["BUTTON"][lang], callback_data="reg_passed_event_" + event))

    if counter == 0:
        bot.send_message(message.chat.id, text=vocab['no_passed_events'],
                         reply_markup=gen_passed_future_reply_markup(vocab))
    else:
        bot.send_message(message.chat.id, text=vocab['select_event'], reply_markup=reply_markup)
    db.close()


@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'future_events'] or message.text == json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')[
                             'future_events'])
def future_events_button(message):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    delete_prev_inline(message)
    events = json.loads(open('events.json', encoding='utf-16').read(), encoding='utf-16')
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')

    reply_markup = InlineKeyboardMarkup()
    counter = 0
    for event in events:
        if events[event]["PASSED"][lang] == 'False':
            counter += 1
            reply_markup.add(
                InlineKeyboardButton(events[event]["BUTTON"][lang], callback_data="reg_future_event_" + event))

    if counter == 0:
        bot.send_message(message.chat.id, text=vocab['no_future_events'],
                         reply_markup=gen_passed_future_reply_markup(vocab))
    else:
        bot.send_message(message.chat.id, text=vocab['select_event'], reply_markup=reply_markup)

    db.close()


@bot.callback_query_handler(func=lambda call: call.data[:17] == "reg_passed_event_")
@bot.callback_query_handler(func=lambda call: call.data[:17] == "reg_future_event_")
def show_event(call):
    try:
        db = dbworker.SQLighter(config.db_file)
        lang = db.get_language(call.from_user.id)
        events = json.loads(open('events.json', encoding='utf-16').read(), encoding='utf-16')
        vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
            "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')

        bot.edit_message_reply_markup(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      reply_markup=InlineKeyboardMarkup())

        if events[call.data[17:]]["PASSED"][lang] == 'True':

            reply_markup = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
            reply_markup.add(vocab['back_to_menu'])
        else:

            reply_markup = ReplyKeyboardMarkup(resize_keyboard=True)
            reply_markup.add(vocab['start_registration'], vocab['back_to_menu'])

        bot.send_message(chat_id=call.message.chat.id, text=events[call.data[17:]]["BUTTON"][lang],
                         parse_mode='Markdown', reply_markup=reply_markup)

        if events[call.data[17:]]["PHOTO"][lang]:
            if events[call.data[17:]]["LINK"][lang] != '':
                keyboard = InlineKeyboardMarkup()
                keyboard.add(
                    InlineKeyboardButton(text=vocab['get_photos_from_url'],
                                         url=events[call.data[17:]]["LINK"][lang]))

                bot.send_photo(chat_id=call.message.chat.id, photo=events[call.data[17:]]["PHOTO"][lang],
                               caption=events[call.data[17:]]["TEXT"][lang], reply_markup=keyboard,
                               parse_mode='Markdown')
            else:
                bot.send_photo(chat_id=call.message.chat.id, photo=events[call.data[17:]]["PHOTO"][lang],
                               caption=events[call.data[17:]]["TEXT"][lang], parse_mode='Markdown')
        else:

            if events[call.data[17:]]["LINK"][lang] != '':
                keyboard = InlineKeyboardMarkup()
                keyboard.add(
                    InlineKeyboardButton(text=vocab['get_photos_from_url'],
                                         url=events[call.data[17:]]["LINK"][lang]))

                bot.send_message(chat_id=call.message.chat.id, text=events[call.data[17:]]["TEXT"][lang],
                                 reply_markup=keyboard, parse_mode='Markdown')
            else:
                bot.send_message(chat_id=call.message.chat.id, text=events[call.data[17:]]["TEXT"][lang],
                                 parse_mode='Markdown')

        if events[call.data[17:]]["FILE"][lang]:
            bot.send_document(chat_id=call.message.chat.id, data=events[call.data[17:]]["FILE"][lang],
                              parse_mode='Markdown')

        bot.register_next_step_handler_by_chat_id(call.message.chat.id, start_event_registration, event=call.data[17:],
                                                  passed=events[call.data[17:]]["PASSED"][lang])
        db.close()
        bot.answer_callback_query(call.id)
    except Exception as e:
        bot.send_message(call.message.chat.id, text=e)


@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'reg_org'] or message.text == json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')[
                             'reg_org'])
def organization_registration(message):
    db = dbworker.SQLighter(config.db_file)
    delete_prev_inline(message)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    reply = ReplyKeyboardMarkup(resize_keyboard=True)
    reply.add(KeyboardButton(text=vocab['cancel']))
    bot.send_message(message.chat.id, text=vocab['input_name'], reply_markup=reply, parse_mode='Markdown')
    bot.register_next_step_handler_by_chat_id(chat_id=message.chat.id, callback=process_name_step,
                                              type=config.CONV_type.REGISTER_ORG)
    db.close()


def start_event_registration(message, event, passed):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    if message.text == vocab['start_registration']:
        reply = ReplyKeyboardMarkup(resize_keyboard=True)
        reply.add(KeyboardButton(vocab['cancel']))
        bot.send_message(message.chat.id, text=vocab['input_name'], reply_markup=reply, parse_mode='Markdown')
        bot.register_next_step_handler_by_chat_id(chat_id=message.chat.id, callback=process_name_step,
                                                  type=config.CONV_type.REGISTER_EVENT, event=event)
    else:
        if passed == 'False':
            bot.send_message(message.chat.id, text=vocab['registration_canceled'], reply_markup=ReplyKeyboardRemove())
        db.set_state(message.from_user.id, config.States.S_MAIN_MENU.value)
        send_main_menu(message, vocab)
    db.close()


# -----------------------------------------------------------------------------------------------------------------------

@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'change_lang'] or message.text == json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')[
                             'change_lang'])
def change_lang(message):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    delete_prev_inline(message)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    reply_markup = ReplyKeyboardMarkup(resize_keyboard=True)
    reply_markup.row_width = 2
    reply_markup.add(KeyboardButton("Қазақ тілінде"),
                     KeyboardButton("На русском языке"))
    bot.send_message(message.from_user.id, text=vocab['select_lang'], reply_markup=reply_markup)
    db.close()


@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'medical'] or message.text == json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')[
                             'medical'])
def medical_list(message):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')

    reply_markup = ReplyKeyboardMarkup(resize_keyboard=True)
    reply_markup.row_width = 2
    reply_markup.row(KeyboardButton(vocab['medical_center']))
    reply_markup.row(KeyboardButton(vocab['medical_points']))
    reply_markup.add(KeyboardButton(vocab['psychology_help']), (KeyboardButton(vocab['religious_help'])))
    reply_markup.row(KeyboardButton(vocab['inform']))

    bot.send_message(chat_id=message.from_user.id, text=vocab['select'], reply_markup=reply_markup)

    db.close()


@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'medical_points'] or message.text ==
                         json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')[
                             'medical_points'])
def process_medical_points(message):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')

    info = json.loads(open('info.json', encoding='utf-16').read())

    if info['med_points']['File'] == 'True':
        print(info['med_points'][lang])
        bot.send_document(chat_id=message.from_user.id, data=info['med_points'][lang])


@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'medical_center'] or message.text ==
                         json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')[
                             'medical_center'])
def process_medical_center(message):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')

    info = json.loads(open('info.json', encoding='utf-16').read())

    bot.send_message(chat_id=message.from_user.id, text=info['med_center'][lang], parse_mode='Markdown')


@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'religious_help'] or message.text ==
                         json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')[
                             'religious_help'])
def religious_help(message):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')

    info = json.loads(open('info.json', encoding='utf-16').read())

    if info['rel_help']['Photo']['File'] == 'True':
        bot.send_photo(chat_id=message.chat.id, photo=info['rel_help']['Photo'][lang])
    bot.send_message(chat_id=message.chat.id, text=info['rel_help']['Info'][lang])
    if info['rel_help']['Doc']['File'] == 'True':
        bot.send_document(chat_id=message.from_user.id, data=info['rel_help']['Doc'][lang])

    reply = ReplyKeyboardMarkup(resize_keyboard=True)
    reply.add(KeyboardButton(vocab['cancel']))
    bot.send_message(message.from_user.id, text=vocab['input_name'], reply_markup=reply, parse_mode='Markdown')
    bot.register_next_step_handler_by_chat_id(chat_id=message.from_user.id, callback=process_name_step,
                                              type=config.CONV_type.MAKE_APPOINTMENT_TO_REL)
    db.close()


@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'psychology_help'] or message.text ==
                         json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')[
                             'psychology_help'])
def psychology_help(message):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')

    info = json.loads(open('info.json', encoding='utf-16').read())

    if info['psy_help']['Photo']['File'] == 'True':
        bot.send_photo(chat_id=message.chat.id, photo=info['psy_help']['Photo'][lang])
    bot.send_message(chat_id=message.chat.id, text=info['psy_help']['Info'][lang])
    if info['psy_help']['Doc']['File'] == 'True':
        bot.send_document(chat_id=message.from_user.id, data=info['psy_help']['Doc'][lang])

    reply = ReplyKeyboardMarkup(resize_keyboard=True)
    reply.add(KeyboardButton(vocab['cancel']))
    bot.send_message(message.from_user.id, text=vocab['input_name'], reply_markup=reply, parse_mode='Markdown')
    bot.register_next_step_handler_by_chat_id(chat_id=message.from_user.id, callback=process_name_step,
                                              type=config.CONV_type.MAKE_APPOINTMENT_TO_PSY)
    db.close()


# -------------------------------------------------ВНЕСТИ_ПРЕДЛОЖЕНИЕ----------------------------------------------------
@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'suggest'] or message.text == json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')[
                             'suggest'])
def suggest(message):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    delete_prev_inline(message)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')

    reply = ReplyKeyboardMarkup(resize_keyboard=True)
    reply.add(KeyboardButton(vocab['cancel']))
    bot.send_message(message.from_user.id, text=vocab['input_name'], reply_markup=reply, parse_mode='Markdown')
    bot.register_next_step_handler_by_chat_id(chat_id=message.chat.id, callback=process_name_step,
                                              type=config.CONV_type.ADD_SUGGESTION)
    db.close()


def process_name_step(message, type, **kwargs):
    try:
        db = dbworker.SQLighter(config.db_file)
        lang = db.get_language(message.from_user.id)
        vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
            "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
        if message.text == vocab['cancel']:
            db.set_state(message.from_user.id, config.States.S_MAIN_MENU.value)
            send_main_menu(message, vocab)
            db.close()
            return
        name = message.text

        reply = ReplyKeyboardMarkup(resize_keyboard=True)
        reply.add(KeyboardButton(text=vocab['cancel']))

        bot.send_message(message.chat.id, text=vocab['input_mail'], reply_markup=reply)

        if type == config.CONV_type.REGISTER_EVENT:
            bot.register_next_step_handler_by_chat_id(message.chat.id, process_mail_step,
                                                      user_data=[kwargs['event'], name], type=type)
        else:
            bot.register_next_step_handler_by_chat_id(message.chat.id, process_mail_step,
                                                      user_data=[name], type=type)
        db.close()
    except Exception as e:
        bot.reply_to(message, e)


def process_mail_step(message, user_data, type):
    try:
        db = dbworker.SQLighter(config.db_file)
        lang = db.get_language(message.from_user.id)
        vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
            "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
        if message.text == vocab['cancel']:
            db.set_state(message.from_user.id, config.States.S_MAIN_MENU.value)
            send_main_menu(message, vocab)
            db.close()
            return
        chat_id = message.chat.id
        mail = message.text
        if type == config.CONV_type.ADD_SUGGESTION:
            markup = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
            markup.add(KeyboardButton(text=vocab['reply_button'], request_contact=True))
            markup.add(KeyboardButton(text=vocab['cancel']))
            bot.send_message(chat_id=chat_id, text=vocab['reply_contact'], reply_markup=markup, parse_mode='Markdown')
            user_data.append(mail)
            bot.register_next_step_handler_by_chat_id(message.chat.id, process_contact_step, user_data=user_data,
                                                      type=type)
        elif type == config.CONV_type.MAKE_APPOINTMENT_TO_PSY or type == config.CONV_type.MAKE_APPOINTMENT_TO_REL:
            markup = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)

            markup.add(KeyboardButton(text=vocab['cancel']))

            bot.send_message(message.chat.id, text=vocab['input_facult'], reply_markup=markup)

            user_data.append(mail)
            bot.register_next_step_handler_by_chat_id(message.chat.id, process_facult_step, user_data=user_data,
                                                      type=type)
        elif type == config.CONV_type.REGISTER_ORG or type == config.CONV_type.REGISTER_EVENT:
            markup = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
            markup.add(KeyboardButton(text=vocab['cancel']))
            bot.send_message(message.chat.id, text=vocab['input_facult'], reply_markup=markup)
            user_data.append(mail)
            bot.register_next_step_handler_by_chat_id(message.chat.id, process_facult_step, user_data=user_data,
                                                      type=type)
        db.close()
    except Exception as e:
        bot.reply_to(message, 'oooops')


def process_facult_step(message, user_data, type):
    try:
        facult = message.text
        chat_id = message.chat.id
        db = dbworker.SQLighter(config.db_file)
        lang = db.get_language(message.from_user.id)
        vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
            "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
        if message.text == vocab['cancel']:
            send_main_menu(message, vocab)
            db.set_state(message.from_user.id, config.States.S_MAIN_MENU.value)
            db.close()
            return
        if type == config.CONV_type.REGISTER_ORG:
            markup = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
            orgs = json.loads(open('organizations.json', encoding='utf-16').read(), encoding='utf-16')
            orgs_names = [v["BUTTON"][lang] for v in orgs["jun"].values()] + [v["BUTTON"][lang] for v in
                                                                              orgs["art"].values()]
            for name in orgs_names:
                markup.add(name)
            markup.add(vocab['cancel'])
            bot.send_message(message.chat.id, text=vocab['select_org'], reply_markup=markup)
            user_data.append(facult)
            bot.register_next_step_handler_by_chat_id(message.chat.id, process_organization_step, user_data=user_data,
                                                      type=type)
        elif type == config.CONV_type.MAKE_APPOINTMENT_TO_PSY or type == config.CONV_type.MAKE_APPOINTMENT_TO_REL:
            markup = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
            markup.add(KeyboardButton(text=vocab['reply_button'], request_contact=True))
            markup.add(KeyboardButton(text=vocab['cancel']))
            bot.send_message(chat_id=chat_id, text=vocab['reply_contact'], reply_markup=markup, parse_mode='Markdown')
            user_data.append(facult)
            bot.register_next_step_handler_by_chat_id(message.chat.id, process_contact_step, user_data=user_data,
                                                      type=type)
        elif type == config.CONV_type.REGISTER_EVENT:
            markup = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
            markup.add(KeyboardButton(text=vocab['reply_button'], request_contact=True))
            markup.add(KeyboardButton(text=vocab['cancel']))
            bot.send_message(chat_id=chat_id, text=vocab['reply_contact'], reply_markup=markup, parse_mode='Markdown')
            user_data.append(facult)
            bot.register_next_step_handler_by_chat_id(message.chat.id, process_contact_step, user_data=user_data,
                                                      type=type)
        db.close()
    except Exception as e:
        bot.reply_to(message, e)


def process_organization_step(message, user_data, type):
    try:
        org = message.text
        db = dbworker.SQLighter(config.db_file)
        lang = db.get_language(message.from_user.id)
        vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
            "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
        if message.text == vocab['cancel']:
            send_main_menu(message, vocab)
            db.set_state(message.from_user.id, config.States.S_MAIN_MENU.value)
            db.close()
            return
        if type == config.CONV_type.REGISTER_ORG:
            markup = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
            markup.add(KeyboardButton(text=vocab['reply_button'], request_contact=True))
            markup.add(KeyboardButton(text=vocab['cancel']))
            bot.send_message(chat_id=message.chat.id, text=vocab['reply_contact'], reply_markup=markup,
                             parse_mode='Markdown')
            user_data.append(org)
            bot.register_next_step_handler_by_chat_id(message.chat.id, process_contact_step, user_data=user_data,
                                                      type=type)
            db.close()
    except Exception as e:
        bot.reply_to(message, 'oooops')


def process_contact_step(message, user_data, type):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    if message.text == vocab['cancel']:
        send_main_menu(message, vocab)
        db.set_state(message.from_user.id, config.States.S_MAIN_MENU.value)
        db.close()
        return
    chat_id = message.chat.id
    number = message.contact.phone_number
    if type == config.CONV_type.ADD_SUGGESTION:
        reply = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
        reply.add(KeyboardButton(text=vocab['cancel']))
        bot.send_message(chat_id=chat_id, text=vocab['input_suggestions'], reply_markup=reply)
        user_data.append(number)
        bot.register_next_step_handler_by_chat_id(message.chat.id, process_suggestion_step, user_data=user_data)
    elif type == config.CONV_type.MAKE_APPOINTMENT_TO_PSY or type == config.CONV_type.MAKE_APPOINTMENT_TO_REL:
        reply = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
        reply.add(KeyboardButton(text=vocab['no_comments']))
        reply.add(KeyboardButton(text=vocab['cancel']))
        bot.send_message(chat_id=chat_id, text=vocab['input_comment'], reply_markup=reply)
        user_data.append(number)
        bot.register_next_step_handler_by_chat_id(message.chat.id, finalize_appointment, user_data=user_data,
                                                  type=type)
    elif type == config.CONV_type.REGISTER_ORG:
        db.add_registration(type="ORG", user_name=user_data[0], mail=user_data[1],
                            facult=user_data[2], contact=number, org=user_data[3])
        reply_markup = ReplyKeyboardMarkup(resize_keyboard=True)
        reply_markup.row(KeyboardButton(vocab['about']),
                         KeyboardButton(vocab['inform']))
        reply_markup.row(KeyboardButton(vocab['suggest']),
                         KeyboardButton(vocab['events']))
        reply_markup.row(KeyboardButton(vocab['organizations']),
                         KeyboardButton(vocab['change_lang']))
        bot.send_message(chat_id=chat_id, text=vocab['final_registr_org'].format(user_data[3]),
                         reply_markup=reply_markup)
    elif type == config.CONV_type.REGISTER_EVENT:
        events = json.loads(open('events.json', encoding='utf-16').read(), encoding='utf-16')
        reply_markup = ReplyKeyboardMarkup(resize_keyboard=True)
        reply_markup.row(KeyboardButton(vocab['about']),
                         KeyboardButton(vocab['inform']))
        reply_markup.row(KeyboardButton(vocab['suggest']),
                         KeyboardButton(vocab['events']))
        reply_markup.row(KeyboardButton(vocab['organizations']),
                         KeyboardButton(vocab['change_lang']))
        db.add_registration(type="EVENT", user_name=user_data[1], mail=user_data[2],
                            facult=user_data[3], contact=number, org=events[user_data[0]]["BUTTON"][lang])
        bot.send_message(chat_id=chat_id, text=vocab['final_registr_event'], reply_markup=reply_markup)
    db.close()


def process_suggestion_step(message, user_data):
    chat_id = message.chat.id
    suggestion = message.text
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    if message.text == vocab['cancel']:
        send_main_menu(message, vocab)
        db.set_state(message.from_user.id, config.States.S_MAIN_MENU.value)
        db.close()
        return
    markup = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    markup.add(KeyboardButton(vocab['without_photo']))
    markup.add(KeyboardButton(vocab['cancel']))
    bot.send_message(chat_id, text=vocab['add_photo'], reply_markup=markup)
    user_data.append(suggestion)
    bot.register_next_step_handler_by_chat_id(message.chat.id, finalize_suggestion, user_data=user_data)
    db.close()


def finalize_appointment(message, user_data, type):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    if message.text == vocab['cancel']:
        send_main_menu(message, vocab)
        db.set_state(message.from_user.id, config.States.S_MAIN_MENU.value)
        db.close()
        return
    no_comment = ""
    comment = message.text
    if comment == vocab['no_comments']:
        user_data.append(no_comment)
    else:
        user_data.append(comment)

    if type == config.CONV_type.MAKE_APPOINTMENT_TO_PSY:
        db.add_psy_appointment(user_name=user_data[0], faculty=user_data[2], contact=user_data[3], mail=user_data[1],
                               comment=user_data[4])
    else:
        db.add_rel_appointment(user_name=user_data[0], faculty=user_data[2], contact=user_data[3], mail=user_data[1],
                               comment=user_data[4])
    bot.send_message(message.chat.id, text=vocab['final_appointment'])
    send_main_menu(message, vocab)
    db.close()


def finalize_suggestion(message, user_data):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    if message.text == vocab['cancel']:
        send_main_menu(message, vocab)
        db.set_state(message.from_user.id, config.States.S_MAIN_MENU.value)
        db.close()
        return
    file = None
    if vocab['without_photo'] != message.text:
        file_info = bot.get_file(message.photo[0].file_id)
        file = bot.download_file(file_info.file_path)

    db.add_suggestion(user_name=user_data[0], suggestions=user_data[3], contact=user_data[2],
                      mail=user_data[1], photo=file)
    bot.send_message(message.chat.id, text=vocab['final_suggestion'])
    send_main_menu(message, vocab)
    db.close()


# ----------------------------------------------------АДМИКА-------------------------------------------------------------

@bot.message_handler(commands=["admin"])
def cmd_admin(message):
    delete_prev_inline(message)
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    user = message.from_user.id
    if db.is_admin(user):
        bot.send_message(message.chat.id, text=vocab['select'], reply_markup=admin_menu(lang))
    else:
        bot.send_message(message.chat.id, text=vocab['not_admin'])
    db.close()


def red_admin_menu(vocab):
    reply = ReplyKeyboardMarkup(resize_keyboard=True)
    reply.add(KeyboardButton(vocab['add_new_admin']))
    reply.add(KeyboardButton(vocab['delete_admin']))
    reply.add(KeyboardButton(vocab['back_to_admin']))
    return reply


@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'red_admin'] or message.text == json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')[
                             'red_admin'])
def admin_red_menu(message):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    user = message.from_user.id
    if not db.is_admin(user):
        bot.send_message(message.chat.id, text=vocab['not_admin'])
        db.close()
        return
    bot.send_message(message.chat.id, text=vocab['select'], reply_markup=red_admin_menu(vocab))
    db.close()


@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'add_new_admin'] or message.text == json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')[
                             'add_new_admin'])
def admin_add(message):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    user = message.from_user.id
    if not db.is_admin(user):
        bot.send_message(message.chat.id, text=vocab['not_admin'])
        db.close()
        return
    reply = ReplyKeyboardMarkup(resize_keyboard=True)
    reply.add(KeyboardButton(text=vocab['cancel']))
    bot.send_message(message.chat.id, text=vocab['add_admin_phone'], reply_markup=reply)
    bot.register_next_step_handler_by_chat_id(chat_id=message.chat.id, callback=process_add_number)
    db.close()


@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'delete_admin'] or message.text == json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')[
                             'delete_admin'])
def delete_admin_list(message):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    user = message.from_user.id
    if not db.is_admin(user):
        bot.send_message(message.chat.id, text=vocab['not_admin'])
        db.close()
        return
    admins_list = db.get_admins()
    if admins_list is None:
        bot.send_message(message.chat.id, text=vocab['no_admins'])
    reply = ReplyKeyboardMarkup(resize_keyboard=True)
    for i in admins_list:
        reply.add(KeyboardButton(i))
    reply.add(vocab['cancel'])
    bot.send_message(message.chat.id, vocab['select_number'], reply_markup=reply)
    bot.register_next_step_handler_by_chat_id(message.chat.id, callback=process_delete_admin)
    db.close()


def process_delete_admin(message):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    admins_list = db.get_admins()
    if message.text == vocab['cancel']:
        bot.send_message(message.chat.id, text=vocab['select'], reply_markup=red_admin_menu(vocab))
    elif message.text in admins_list:
        db.delete_admin(number=message.text)
        bot.send_message(message.chat.id, text=vocab['admin_deleted'], reply_markup=red_admin_menu(vocab))
    else:
        bot.send_message(message.chat.id, vocab['select_number'])
        bot.register_next_step_handler_by_chat_id(message.chat.id, callback=process_delete_admin)
    db.close()


@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'red_menu'] or message.text == json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')[
                             'red_menu'])
def admin_red_menu(message):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    user = message.from_user.id
    if not db.is_admin(user):
        bot.send_message(message.chat.id, text=vocab['not_admin'])
        db.close()
        return
    reply = generate_red_info_menu(lang, vocab)
    bot.send_message(message.chat.id, vocab['select'], reply_markup=reply)
    db.close()


def generate_red_info_menu(lang, vocab):
    reply = ReplyKeyboardMarkup(resize_keyboard=True)
    reply.add(KeyboardButton(vocab['red_info']))
    reply.add(KeyboardButton(vocab['red_projects']))
    reply.add(KeyboardButton(vocab['red_organizations_jun']))
    reply.add(KeyboardButton(vocab['red_organizations_art']))
    reply.add(KeyboardButton(vocab['red_events']))
    reply.add(KeyboardButton(vocab['back_to_admin']))
    return reply


def generate_red_info_buttons(lang, vocab):
    reply = ReplyKeyboardMarkup(resize_keyboard=True)
    reply.add(KeyboardButton(vocab['file']), KeyboardButton(vocab['text']))
    reply.add(KeyboardButton(vocab['cancel']))
    return reply


def generate_red_info_buttons_lang(lang, vocab):
    reply = ReplyKeyboardMarkup(resize_keyboard=True)
    reply.add(KeyboardButton('KZ'))
    reply.add(KeyboardButton('RU'))
    reply.add(KeyboardButton(vocab['cancel']))
    return reply


@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'red_info'] or message.text == json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')[
                             'red_info'])
def red_info_menu(message):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    user = message.from_user.id
    if not db.is_admin(user):
        bot.send_message(message.chat.id, text=vocab['not_admin'])
        db.close()
        return
    info = [
        vocab['mission'], vocab['rule'], vocab['work'], vocab['stud_house'], vocab['library'],
        vocab['sport_clubs'], vocab['army'], vocab['medical_center'], vocab['medical_points'],
        vocab['psychology_help'], vocab['religious_help'], vocab['contacts'], vocab['cancel']]

    reply = ReplyKeyboardMarkup(resize_keyboard=True)
    for inf in info:
        reply.add(inf)
    bot.send_message(message.chat.id, vocab['select'], reply_markup=reply)
    bot.register_next_step_handler_by_chat_id(message.chat.id, red_info)
    db.close()


def red_info(message):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    if message.text == vocab['cancel']:
        reply = generate_red_info_menu(lang, vocab)
        bot.send_message(message.chat.id, vocab['select'], reply_markup=reply)
        db.close()
        return
    dict_info = {
        vocab['mission']: "mission",
        vocab['rule']: "rule",
        vocab['work']: "work",
        vocab['stud_house']: "stud_house",
        vocab['library']: "library",
        vocab['sport_clubs']: "sport_clubs",
        vocab['army']: "army",
        vocab['medical_center']: "med_center",
        vocab['medical_points']: "med_points"
    }
    if message.text in dict_info.keys():
        category = dict_info[message.text]
        bot.send_message(message.chat.id, text=vocab['select_type_of_message'],
                         reply_markup=generate_red_info_buttons(lang, vocab))

        bot.register_next_step_handler_by_chat_id(message.chat.id, callback=red_info_cat_step, data=[category])
    elif message.text == vocab['contacts']:
        bot.send_message(message.chat.id, text=vocab['select'], reply_markup=red_contact_menu(vocab))
        bot.register_next_step_handler_by_chat_id(message.chat.id, red_contact_step)
    elif message.text == vocab['psychology_help'] or message.text == vocab['religious_help']:
        dict_help_info = {
            vocab['psychology_help']: "psy_help",
            vocab['religious_help']: "rel_help",
        }
        category = dict_help_info[message.text]
        bot.send_message(message.chat.id, text=vocab['select'], reply_markup=generate_red_info_buttons_for_help(vocab))
        bot.register_next_step_handler_by_chat_id(message.chat.id, callback=red_info_cat_step, data=[category])
    else:
        info = [vocab['mission'], vocab['rule'], vocab['work'], vocab['stud_house'], vocab['library'],
                vocab['sport_clubs'], vocab['army'], vocab['medical_center'], vocab['medical_points'], vocab['cancel']]
        reply = ReplyKeyboardMarkup(resize_keyboard=True)
        for inf in info:
            reply.add(inf)
        bot.send_message(message.chat.id, vocab['select'], reply_markup=reply)
        bot.register_next_step_handler_by_chat_id(message.chat.id, red_info)
    db.close()


def generate_red_info_buttons_for_help(vocab):
    reply = ReplyKeyboardMarkup(resize_keyboard=True)
    reply.add(KeyboardButton(vocab['file']), KeyboardButton(vocab['text']), KeyboardButton(vocab['image']))
    reply.add(KeyboardButton(vocab['cancel']))
    return reply


def red_contact_menu(vocab):
    reply_markup = ReplyKeyboardMarkup(resize_keyboard=True)
    reply_markup.add(KeyboardButton(vocab['red_site_ru']), KeyboardButton(vocab['red_site_kz']))
    reply_markup.add(KeyboardButton(vocab['add_number_ru']), KeyboardButton(vocab['add_number_kz']))
    reply_markup.add(KeyboardButton(vocab['delete_number_ru']), KeyboardButton(vocab['delete_number_kz']))
    reply_markup.add(KeyboardButton(vocab['red_address_ru']), KeyboardButton(vocab['red_address_kz']))
    reply_markup.add(KeyboardButton(vocab['red_coordinates_ru']), KeyboardButton(vocab['red_coordinates_kz']))
    reply_markup.add(KeyboardButton(vocab['cancel']))
    return reply_markup


def red_contact_step(message):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    if message.text == vocab['cancel']:
        reply = generate_red_info_menu(lang, vocab)
        bot.send_message(message.chat.id, vocab['select'], reply_markup=reply)
        db.close()
        return
    mess = ['select']
    reply = ReplyKeyboardMarkup(resize_keyboard=True)

    if message.text == vocab['red_site_ru']:
        mess = vocab['input_site_ru']
        bot.register_next_step_handler_by_chat_id(chat_id=message.chat.id, callback=finish_red_contact_step,
                                                  status=config.RED_contact.ADD_SITE, lang_red='RU')
    elif message.text == vocab['add_number_ru']:
        mess = vocab['input_number_with_text_ru']
        bot.register_next_step_handler_by_chat_id(chat_id=message.chat.id, callback=finish_red_contact_step,
                                                  status=config.RED_contact.ADD_NUMBER, lang_red='RU')
    elif message.text == vocab['delete_number_ru']:
        info = json.loads(open('info.json', encoding='utf-16').read(), encoding='utf-16')
        counter = 0
        for number in info['contacts']['RU']['telephones']:
            reply.add(KeyboardButton(number))
            counter += 1
        if counter == 0:
            bot.send_message(message.chat.id, text=vocab['no_numbers'], reply_markup=red_contact_menu(vocab))
            bot.register_next_step_handler_by_chat_id(message.chat.id, red_contact_step)
            db.close()
            return
        bot.register_next_step_handler_by_chat_id(chat_id=message.chat.id, callback=finish_red_contact_step,
                                                  status=config.RED_contact.DELETE_NUMBER, lang_red='RU')
        mess = vocab['delete_number_mess_ru']
    elif message.text == vocab['red_address_ru']:
        bot.register_next_step_handler_by_chat_id(chat_id=message.chat.id, callback=finish_red_contact_step,
                                                  status=config.RED_contact.ADDRESS, lang_red='RU')
        mess = vocab['red_address_text_ru']
    elif message.text == vocab['red_coordinates_ru']:
        bot.register_next_step_handler_by_chat_id(chat_id=message.chat.id, callback=finish_red_contact_step,
                                                  status=config.RED_contact.RED_LOC, lang_red='RU')
        mess = vocab['input_coordinates_ru']
    elif message.text == vocab['red_site_kz']:
        mess = vocab['input_site_kz']
        bot.register_next_step_handler_by_chat_id(chat_id=message.chat.id, callback=finish_red_contact_step,
                                                  status=config.RED_contact.ADD_SITE, lang_red='KZ')
    elif message.text == vocab['add_number_kz']:
        mess = vocab['input_number_with_text_kz']
        bot.register_next_step_handler_by_chat_id(chat_id=message.chat.id, callback=finish_red_contact_step,
                                                  status=config.RED_contact.ADD_NUMBER, lang_red='KZ')
    elif message.text == vocab['delete_number_kz']:
        info = json.loads(open('info.json', encoding='utf-16').read(), encoding='utf-16')
        counter = 0
        for number in info['contacts']['KZ']['telephones']:
            counter += 1
            reply.add(KeyboardButton(number))
        if counter == 0:
            bot.send_message(message.chat.id, text=vocab['no_numbers'], reply_markup=red_contact_menu(vocab))
            bot.register_next_step_handler_by_chat_id(message.chat.id, red_contact_step)
            db.close()
            return
        bot.register_next_step_handler_by_chat_id(chat_id=message.chat.id, callback=finish_red_contact_step,
                                                  status=config.RED_contact.DELETE_NUMBER, lang_red='KZ')
    elif message.text == vocab['red_address_kz']:
        bot.register_next_step_handler_by_chat_id(chat_id=message.chat.id, callback=finish_red_contact_step,
                                                  status=config.RED_contact.ADDRESS, lang_red='KZ')
        mess = vocab['red_address_text_kz']
    elif message.text == vocab['red_coordinates_kz']:
        bot.register_next_step_handler_by_chat_id(chat_id=message.chat.id, callback=finish_red_contact_step,
                                                  status=config.RED_contact.RED_LOC, lang_red='KZ')
        mess = vocab['input_coordinates_kz']
    else:
        bot.send_message(message.chat.id, text=vocab['select'], reply_markup=red_contact_menu(vocab))
        bot.register_next_step_handler_by_chat_id(message.chat.id, red_contact_step)
        db.close()
        return
    db.close()
    reply.add(KeyboardButton(vocab['cancel']))
    bot.send_message(chat_id=message.chat.id, text=mess, reply_markup=reply)


def finish_red_contact_step(message, status, lang_red):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    info = json.loads(open('info.json', encoding='utf-16').read(), encoding='utf-16')
    if message.text == vocab['cancel']:
        bot.send_message(message.chat.id, text=vocab['select'], reply_markup=red_contact_menu(vocab))
        bot.register_next_step_handler_by_chat_id(message.chat.id, red_contact_step)
        db.close()
        return
    if status == config.RED_contact.RED_LOC:
        loc = list(map(float, message.text.split(',')))
        if len(loc) == 2:
            info['contacts'][lang_red]['latitude'] = loc[0]
            info['contacts'][lang_red]['longitude'] = loc[1]
            with open('info.json', 'w', encoding='utf-16') as outfile:
                json.dump(info, outfile, ensure_ascii=False)
            bot.send_message(message.chat.id, text=vocab['changes_saved'], reply_markup=red_contact_menu(vocab))
            bot.register_next_step_handler_by_chat_id(message.chat.id, red_contact_step)
    elif status == config.RED_contact.ADDRESS:
        info['contacts'][lang_red]['address'] = message.text
        with open('info.json', 'w', encoding='utf-16') as outfile:
            json.dump(info, outfile, ensure_ascii=False)
        bot.send_message(message.chat.id, text=vocab['changes_saved'], reply_markup=red_contact_menu(vocab))
        bot.register_next_step_handler_by_chat_id(message.chat.id, red_contact_step)
    elif status == config.RED_contact.DELETE_NUMBER:
        if message.text in info['contacts'][lang_red]['telephones']:
            info['contacts'][lang_red]['telephones'].remove(message.text)
        with open('info.json', 'w', encoding='utf-16') as outfile:
            json.dump(info, outfile, ensure_ascii=False)
        bot.send_message(message.chat.id, text=vocab['changes_saved'], reply_markup=red_contact_menu(vocab))
        bot.register_next_step_handler_by_chat_id(message.chat.id, red_contact_step)
    elif status == config.RED_contact.ADD_NUMBER:
        info['contacts'][lang_red]['telephones'].append(message.text)
        with open('info.json', 'w', encoding='utf-16') as outfile:
            json.dump(info, outfile, ensure_ascii=False)
        bot.send_message(message.chat.id, text=vocab['changes_saved'], reply_markup=red_contact_menu(vocab))
        bot.register_next_step_handler_by_chat_id(message.chat.id, red_contact_step)
    elif status == config.RED_contact.ADD_SITE:
        info['contacts'][lang_red]['site'] = message.text
        with open('info.json', 'w', encoding='utf-16') as outfile:
            json.dump(info, outfile, ensure_ascii=False)
        bot.send_message(message.chat.id, text=vocab['changes_saved'], reply_markup=red_contact_menu(vocab))
        bot.register_next_step_handler_by_chat_id(message.chat.id, red_contact_step)


def red_info_cat_step(message, data):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    if message.text == vocab['cancel']:
        reply = generate_red_info_menu(lang, vocab)
        bot.send_message(message.chat.id, vocab['select'], reply_markup=reply)
    elif message.text == vocab['file'] or message.text == vocab['text'] or message.text == vocab['image']:
        bot.send_message(message.chat.id, vocab['select_lang_of_message'],
                         reply_markup=generate_red_info_buttons_lang(lang, vocab))
        data.append(message.text)
        bot.register_next_step_handler_by_chat_id(message.chat.id, callback=red_info_lang_step, data=data)
    else:
        bot.send_message(message.chat.id, text=vocab['select_type_of_message'],
                         reply_markup=generate_red_info_buttons(lang, vocab))
        bot.register_next_step_handler_by_chat_id(message.chat.id, callback=red_info_cat_step, data=data)
    db.close()


def red_info_lang_step(message, data):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    if message.text == vocab['cancel']:
        reply = generate_red_info_menu(lang, vocab)
        bot.send_message(message.chat.id, vocab['select'], reply_markup=reply)
    elif message.text == 'KZ' or message.text == 'RU':
        if data[1] == vocab['text']:
            message_send = vocab['input_text']
        elif data[1] == vocab['file']:
            message_send = vocab['input_file']
        elif data[1] == vocab['image']:
            message_send = vocab['input_photo']

        reply = ReplyKeyboardMarkup(resize_keyboard=True)
        reply.add(vocab['cancel'])
        bot.send_message(message.chat.id, message_send, reply_markup=reply)
        data.append(message.text)
        bot.register_next_step_handler_by_chat_id(message.chat.id, callback=red_info_final_step, data=data)
    else:
        bot.send_message(message.chat.id, vocab['select_lang_of_message'],
                         reply_markup=generate_red_info_buttons_lang(lang, vocab))
        bot.register_next_step_handler_by_chat_id(message.chat.id, callback=red_info_lang_step, data=data)
    db.close()


def red_info_final_step(message, data):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    info = json.loads(open('info.json', encoding='utf-16').read())
    if message.text == vocab['cancel']:
        reply = generate_red_info_menu(lang, vocab)
        bot.send_message(message.chat.id, vocab['select'], reply_markup=reply)
        db.close()
        return
    elif data[1] == vocab['text']:
        cat = data[0]
        lang = data[2]
        info[data[0]]["File"] = "False"
        info[data[0]][lang] = message.text
        with open('info.json', 'w', encoding='utf-16') as outfile:
            json.dump(info, outfile, ensure_ascii=False)
    elif data[1] == vocab['file']:
        cat = data[0]
        lang = data[2]
        if not message.document:
            reply = ReplyKeyboardMarkup(resize_keyboard=True)
            reply.add(vocab['cancel'])
            bot.send_message(message.chat.id, vocab['input_file'], reply_markup=reply)
            bot.register_next_step_handler_by_chat_id(message.chat.id, callback=red_info_final_step, data=data)
            db.close()
            return
        else:
            if data[0] == 'psy_help' or data[0] == 'rel_help':
                info[data[0]]["Doc"]["File"] = "True"
                info[data[0]]["Doc"][lang] = message.document.file_id

                file_info = bot.get_file(message.document.file_id)
                file = requests.get(
                    'https://api.telegram.org/file/bot{0}/{1}'.format(config.token, file_info.file_path))

                with open('info.json', 'w', encoding='utf-16') as outfile:
                    json.dump(info, outfile, ensure_ascii=False)
                with open('info/' + message.document.file_name, 'wb') as outfile:
                    outfile.write(file.content)

            else:
                info[data[0]]["File"] = "True"
                info[data[0]][lang] = message.document.file_id
                file_info = bot.get_file(message.document.file_id)
                file = requests.get(
                    'https://api.telegram.org/file/bot{0}/{1}'.format(config.token, file_info.file_path))

                with open('info.json', 'w', encoding='utf-16') as outfile:
                    json.dump(info, outfile, ensure_ascii=False)
                with open('info/' + message.document.file_name, 'wb') as outfile:
                    outfile.write(file.content)
    elif data[1] == vocab['image']:
        lang = data[2]
        if not message.photo:
            reply = ReplyKeyboardMarkup(resize_keyboard=True)
            reply.add(vocab['cancel'])
            bot.send_message(message.chat.id, vocab['input_photo'], reply_markup=reply)
            bot.register_next_step_handler_by_chat_id(message.chat.id, callback=red_info_final_step, data=data)
            db.close()
            return
        else:
            dic = json.loads(open('info.json', encoding='utf-16').read(), encoding='utf-16')
            name = 'info.json'

            dic[data[0]]['Photo']["File"] = "True"
            dic[data[0]]['Photo'][lang] = message.photo[len(message.photo) - 1].file_id

            with open(name, 'w', encoding='utf-16') as outfile:
                json.dump(dic, outfile, ensure_ascii=False)

    reply = generate_red_info_menu(lang, vocab)
    bot.send_message(message.chat.id, vocab['changes_saved'], reply_markup=reply)
    db.close()


@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'main_menu_admin'] or message.text == json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')[
                             'main_menu_admin'])
def admin_back_main_menu(message):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    user = message.from_user.id
    if not db.is_admin(user):
        bot.send_message(message.chat.id, text=vocab['not_admin'])
        db.close()
        return
    send_main_menu(message, vocab)
    db.close()


# -------------------------------------------------ADMIN->INFO-----------------------------------------------------------
@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'info_admin'] or message.text == json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')[
                             'info_admin'])
def admin_info(message):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    user = message.from_user.id
    if not db.is_admin(user):
        bot.send_message(message.chat.id, text=vocab['not_admin'])
        db.close()
        return
    bot.send_message(message.chat.id, vocab['select'], reply_markup=admin_info_menu(lang))
    bot.register_next_step_handler_by_chat_id(chat_id=message.chat.id, callback=process_admin_info)
    db.close()


def process_add_number(message):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    if message.text == vocab['cancel']:
        bot.send_message(message.chat.id, text=vocab['select'], reply_markup=admin_menu(lang))
    else:
        if message.contact is not None and message.contact.phone_number is not None:
            db.add_admin(config.validate_number(message.contact.phone_number))
            bot.send_message(message.chat.id, text=vocab['admin_added'], reply_markup=admin_menu(lang))
        elif message.text is not None:
            db.add_admin(config.validate_number(message.text))
            bot.send_message(message.chat.id, text=vocab['admin_added'], reply_markup=admin_menu(lang))
        else:
            reply = ReplyKeyboardMarkup(resize_keyboard=True)
            reply.add(KeyboardButton(text=vocab['cancel']))
            bot.send_message(message.chat.id, text=vocab['add_admin_phone'], reply_markup=reply)
            bot.register_next_step_handler_by_chat_id(chat_id=message.chat.id, callback=process_add_number)
    db.close()


def process_admin_info(message):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')

    if message.text == vocab['back_to_admin']:
        bot.send_message(message.chat.id, text=vocab['select'], reply_markup=admin_menu(lang))
        db.close()
        return
    elif message.text == vocab['applic_psy_appointment']:
        filename = db.get_psy_applications()
        if filename != 'EMPTY':
            bot.send_document(message.chat.id, data=open(filename, 'rb'))
            os.remove(filename)
        else:
            bot.send_message(message.chat.id, text=vocab['no_psy_applications'])
        bot.register_next_step_handler_by_chat_id(chat_id=message.chat.id, callback=process_admin_info)
    elif message.text == vocab['applic_rel_appointment']:
        filename = db.get_rel_applications()
        if filename != 'EMPTY':
            bot.send_document(message.chat.id, data=open(filename, 'rb'))
            os.remove(filename)
        else:
            bot.send_message(message.chat.id, text=vocab['no_rel_applications'])
        bot.register_next_step_handler_by_chat_id(chat_id=message.chat.id, callback=process_admin_info)
    elif message.text == vocab['applic_suggestion']:
        filename = db.get_suggestions()
        if filename != 'EMPTY':
            bot.send_document(message.chat.id, data=open(filename, 'rb'))
            os.remove(filename)
        else:
            bot.send_message(message.chat.id, text=vocab['no_suggestions'])
        bot.register_next_step_handler_by_chat_id(chat_id=message.chat.id, callback=process_admin_info)
    elif message.text == vocab['applic_org']:
        markup = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
        orgs = json.loads(open('organizations.json', encoding='utf-16').read(), encoding='utf-16')
        orgs_names = [v["BUTTON"][lang] for v in orgs["jun"].values()] + [v["BUTTON"][lang] for v in
                                                                          orgs["art"].values()]
        for name in orgs_names:
            markup.add(name)
        markup.add(vocab['cancel'])
        bot.send_message(message.chat.id, text=vocab['select_org'], reply_markup=markup)
        bot.register_next_step_handler_by_chat_id(message.chat.id, process_registrations_org)
    elif message.text == vocab['applic_event']:
        markup = ReplyKeyboardMarkup(resize_keyboard=True)
        events = json.loads(open('events.json', encoding='utf-16').read(), encoding='utf-16')
        events_names = [v["BUTTON"][lang] for v in events.values()]
        for name in events_names:
            markup.add(name)
        markup.add(vocab['cancel'])
        bot.send_message(message.chat.id, text=vocab['select_event'], reply_markup=markup)
        bot.register_next_step_handler_by_chat_id(message.chat.id, process_registrations_events)
    db.close()


def process_registrations_org(message):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    if message.text == vocab['cancel']:
        bot.send_message(message.chat.id, vocab['select'], reply_markup=admin_info_menu(lang))
        bot.register_next_step_handler_by_chat_id(chat_id=message.chat.id, callback=process_admin_info)
        db.close()
        return
    orgs = json.loads(open('organizations.json', encoding='utf-16').read(), encoding='utf-16')
    orgs_names = [v["BUTTON"][lang] for v in orgs["jun"].values()] + [v["BUTTON"][lang] for v in
                                                                      orgs["art"].values()]
    if message.text in orgs_names:
        curren_org = (list(filter(lambda v: orgs["jun"][v]["BUTTON"][lang] == message.text, orgs["jun"])) +
                      list(filter(lambda v: orgs["art"][v]["BUTTON"][lang] == message.text, orgs["art"])))[0]
        org_type = 'jun' if curren_org[0] == 'j' else 'art'
        org = orgs[org_type][curren_org]
        filename = db.get_current_org_registraitons(ru_name=org['BUTTON']['RU'], kz_name=org['BUTTON']['KZ'])
        if filename != 'EMPTY':
            bot.send_document(message.chat.id, data=open(filename, 'rb'))
            bot.send_message(message.chat.id, vocab['select'], reply_markup=admin_info_menu(lang))
            bot.register_next_step_handler_by_chat_id(chat_id=message.chat.id, callback=process_admin_info)
            os.remove(filename)
        else:
            bot.send_message(message.chat.id, text=vocab['no_registrations'])
            bot.send_message(message.chat.id, vocab['select'], reply_markup=admin_info_menu(lang))
            bot.register_next_step_handler_by_chat_id(chat_id=message.chat.id, callback=process_admin_info)
    else:
        bot.send_message(message.chat.id, text=vocab['select_org'])
        bot.register_next_step_handler_by_chat_id(message.chat.id, process_registrations_org)
    db.close()


def process_registrations_events(message):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    if message.text == vocab['cancel']:
        bot.send_message(message.chat.id, vocab['select'], reply_markup=admin_info_menu(lang))
        bot.register_next_step_handler_by_chat_id(chat_id=message.chat.id, callback=process_admin_info)
        db.close()
        return
    events = json.loads(open('events.json', encoding='utf-16').read(), encoding='utf-16')
    events_names = [v["BUTTON"][lang] for v in events.values()]
    if message.text in events_names:
        curren_event = list(filter(lambda v: events[v]["BUTTON"][lang] == message.text, events))[0]
        event = events[curren_event]
        filename = db.get_current_event_registraitons(ru_name=event['BUTTON']['RU'], kz_name=event['BUTTON']['KZ'])
        if filename != 'EMPTY':
            bot.send_document(message.chat.id, data=open(filename, 'rb'))
            bot.send_message(message.chat.id, vocab['select'], reply_markup=admin_info_menu(lang))
            bot.register_next_step_handler_by_chat_id(chat_id=message.chat.id, callback=process_admin_info)
            os.remove(filename)
        else:
            bot.send_message(message.chat.id, text=vocab['no_registrations'])
            bot.send_message(message.chat.id, vocab['select'], reply_markup=admin_info_menu(lang))
            bot.register_next_step_handler_by_chat_id(chat_id=message.chat.id, callback=process_admin_info)
    else:
        bot.send_message(message.chat.id, text=vocab['select_event'])
        bot.register_next_step_handler_by_chat_id(message.chat.id, process_registrations_events)
    db.close()


def red_menu(lang, with_photo=False, with_file=False):
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')

    reply = ReplyKeyboardMarkup(resize_keyboard=True)
    reply.add(KeyboardButton(vocab['chang_but_ru']), KeyboardButton(vocab['chang_but_kz']))
    reply.add(KeyboardButton(vocab['chang_txt_ru']), KeyboardButton(vocab['chang_txt_kz']))
    if with_photo:
        reply.add(KeyboardButton(vocab['chang_photo_ru']), KeyboardButton(vocab['chang_photo_kz']))
    if with_file:
        reply.add(KeyboardButton(vocab['chang_file_ru']), KeyboardButton(vocab['chang_file_kz']))
    reply.add(KeyboardButton(vocab['delete']), KeyboardButton(vocab['fin_change']))

    return reply


def red_future_menu(lang, with_photo=False, with_file=False):
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')

    reply = ReplyKeyboardMarkup(resize_keyboard=True)
    reply.add(KeyboardButton(vocab['mark_passed']))
    reply.add(KeyboardButton(vocab['chang_but_ru']), KeyboardButton(vocab['chang_but_kz']))
    reply.add(KeyboardButton(vocab['chang_txt_ru']), KeyboardButton(vocab['chang_txt_kz']))
    if with_photo:
        reply.add(KeyboardButton(vocab['chang_photo_ru']), KeyboardButton(vocab['chang_photo_kz']))
        reply.add(KeyboardButton(vocab['chang_file_ru']), KeyboardButton(vocab['chang_file_kz']))
    if with_file:
        reply.add(KeyboardButton(vocab['chang_file_ru']), KeyboardButton(vocab['chang_file_kz']))
    reply.add(KeyboardButton(vocab['delete']), KeyboardButton(vocab['fin_change']))

    return reply


def red_passed_menu(lang, with_photo=False, with_file=False):
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')

    reply = ReplyKeyboardMarkup(resize_keyboard=True)
    reply.add(KeyboardButton(vocab['change_photo_link']), KeyboardButton(vocab['delete_photo_link']))
    reply.add(KeyboardButton(vocab['chang_but_ru']), KeyboardButton(vocab['chang_but_kz']))
    reply.add(KeyboardButton(vocab['chang_txt_ru']), KeyboardButton(vocab['chang_txt_kz']))
    if with_photo:
        reply.add(KeyboardButton(vocab['chang_photo_ru']), KeyboardButton(vocab['chang_photo_kz']))
        reply.add(KeyboardButton(vocab['chang_file_ru']), KeyboardButton(vocab['chang_file_kz']))
    if with_file:
        reply.add(KeyboardButton(vocab['chang_file_ru']), KeyboardButton(vocab['chang_file_kz']))
    reply.add(KeyboardButton(vocab['delete']), KeyboardButton(vocab['fin_change']))

    return reply


def add_red_menu(lang):
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    reply = ReplyKeyboardMarkup(resize_keyboard=True)
    reply.add(KeyboardButton(vocab['add_new']), vocab['red_prev'])
    reply.add(KeyboardButton(vocab['cancel']))
    return reply


@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'red_projects'] or message.text == json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')[
                             'red_projects'])
def red_project(message):
    type = config.RED_type.PROJECT
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    user = message.from_user.id
    if not db.is_admin(user):
        bot.send_message(message.chat.id, text=vocab['not_admin'])
        db.close()
        return
    reply = add_red_menu(lang)
    bot.send_message(message.chat.id, text=vocab['select'], reply_markup=reply)
    bot.register_next_step_handler_by_chat_id(message.chat.id, callback=red_or_add_step, type=type)


@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'back_to_admin'] or message.text == json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')[
                             'back_to_admin'])
def back_to_admin(message):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    user = message.from_user.id
    if db.is_admin(user):
        bot.send_message(message.chat.id, text=vocab['select'], reply_markup=admin_menu(lang))
    else:
        bot.send_message(message.chat.id, text=vocab['not_admin'])


@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'red_organizations_jun'] or message.text ==
                         json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')[
                             'red_organizations_jun'])
def red_organizations_jun(message):
    type = config.RED_type.ORG_JUN
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    user = message.from_user.id
    if not db.is_admin(user):
        bot.send_message(message.chat.id, text=vocab['not_admin'])
        db.close()
        return
    reply = add_red_menu(lang)
    bot.send_message(message.chat.id, text=vocab['select'], reply_markup=reply)
    bot.register_next_step_handler_by_chat_id(message.chat.id, callback=red_or_add_step, type=type)


@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'red_organizations_art'] or message.text ==
                         json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')[
                             'red_organizations_art'])
def red_organizations_art(message):
    type = config.RED_type.ORG_ART
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    user = message.from_user.id
    if not db.is_admin(user):
        bot.send_message(message.chat.id, text=vocab['not_admin'])
        db.close()
        return
    reply = add_red_menu(lang)
    bot.send_message(message.chat.id, text=vocab['select'], reply_markup=reply)
    bot.register_next_step_handler_by_chat_id(message.chat.id, callback=red_or_add_step, type=type)


def gen_add_pass_fut_markup(vocab):
    reply = ReplyKeyboardMarkup(resize_keyboard=True)
    reply.add(KeyboardButton(vocab['add_new']))
    reply.add(KeyboardButton(vocab['red_passed_events']), KeyboardButton(vocab['red_future_events']))
    reply.add(KeyboardButton(vocab['cancel']))

    return reply


@bot.message_handler(
    func=lambda message: message.text == json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16')[
        'red_events'] or message.text == json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')[
                             'red_events'])
def red_event(message):
    type = config.RED_type.EVENT
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    user = message.from_user.id
    if not db.is_admin(user):
        bot.send_message(message.chat.id, text=vocab['not_admin'])
        db.close()
        return

    reply = gen_add_pass_fut_markup(vocab)

    bot.send_message(message.chat.id, text=vocab['select'], reply_markup=reply)
    bot.register_next_step_handler_by_chat_id(message.chat.id, callback=red_or_add_step, type=type)


def admin_menu(lang):
    reply = ReplyKeyboardMarkup(resize_keyboard=True)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    reply.row_width = 2
    reply.add(KeyboardButton(vocab['red_admin']), KeyboardButton(vocab['info_admin']))
    reply.add(KeyboardButton(vocab['main_menu_admin']), KeyboardButton(vocab['red_menu']))
    return reply


def admin_info_menu(lang):
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    reply = ReplyKeyboardMarkup(resize_keyboard=True)
    reply.add(KeyboardButton(vocab['applic_event']))
    reply.add(KeyboardButton(vocab['applic_suggestion']))
    reply.add(KeyboardButton(vocab['applic_org']))
    reply.add(KeyboardButton(vocab['applic_psy_appointment']),
              KeyboardButton(vocab['applic_rel_appointment']))
    reply.add(KeyboardButton(vocab['back_to_admin']))
    return reply


def choose_variant_step(message, type):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    if vocab['cancel'] == message.text:
        bot.send_message(message.chat.id, vocab['select'], reply_markup=generate_red_info_menu(lang, vocab))
        db.close()

    projects = json.loads(open('projects.json', encoding='utf-16').read(), encoding='utf-16')
    events = json.loads(open('events.json', encoding='utf-16').read(), encoding='utf-16')
    organizations = json.loads(open('organizations.json', encoding='utf-16').read(), encoding='utf-16')
    obj = message.text

    if type == config.RED_type.FUT_EVENT:
        if obj in [events[event]['BUTTON'][lang] for event in events]:
            reply = red_future_menu(lang, with_photo=True)
            bot.send_message(message.chat.id, vocab['select'], reply_markup=reply)
            bot.register_next_step_handler_by_chat_id(message.chat.id,
                                                      callback=choose_what_red_step,
                                                      type=config.RED_type.EVENT,
                                                      obj=obj)
        else:
            bot.send_message(message.chat.id, text=vocab['select_event'])
    if type == config.RED_type.PAS_EVENT:
        if obj in [events[event]['BUTTON'][lang] for event in events]:
            reply = red_passed_menu(lang, with_photo=True)
            bot.send_message(message.chat.id, vocab['select'], reply_markup=reply)

            bot.register_next_step_handler_by_chat_id(message.chat.id,
                                                      callback=choose_what_red_step,
                                                      type=config.RED_type.EVENT,
                                                      obj=obj)
        else:
            bot.send_message(message.chat.id, text=vocab['select_event'])
    elif type == config.RED_type.PROJECT:
        if obj in [projects[proj]['BUTTON'][lang] for proj in projects]:
            reply = red_menu(lang, with_file=True)
            bot.send_message(message.chat.id, vocab['select'], reply_markup=reply)
            bot.register_next_step_handler_by_chat_id(message.chat.id, callback=choose_what_red_step, type=type,
                                                      obj=obj)
        else:
            bot.send_message(message.chat.id, text=vocab['select_project'])
    elif type == config.RED_type.ORG_ART:
        if obj in [organizations['art'][org]['BUTTON'][lang] for org in organizations['art']]:
            reply = red_menu(lang, with_file=True)
            bot.send_message(message.chat.id, vocab['select'], reply_markup=reply)
            bot.register_next_step_handler_by_chat_id(message.chat.id, callback=choose_what_red_step, type=type,
                                                      obj=obj)
        else:
            bot.send_message(message.chat.id, text=vocab['select_org'])
    elif type == config.RED_type.ORG_JUN:
        if obj in [organizations['jun'][org]['BUTTON'][lang] for org in organizations['jun']]:
            reply = red_menu(lang, with_file=True)
            bot.send_message(message.chat.id, vocab['select'], reply_markup=reply)
            bot.register_next_step_handler_by_chat_id(message.chat.id, callback=choose_what_red_step, type=type,
                                                      obj=obj)
        else:
            bot.send_message(message.chat.id, text=vocab['select_org'])
    db.close()


def choose_what_red_step(message, type, obj):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    if message.text == vocab['fin_change'] or message.text == vocab['cancel']:
        bot.send_message(message.chat.id, vocab['select'], reply_markup=generate_red_info_menu(lang, vocab))
        db.close()
    elif message.text == vocab['change_photo_link']:
        lang_ad = ''
        reply = ReplyKeyboardMarkup(resize_keyboard=True)
        bot.send_message(message.chat.id, vocab['input_url_to_photos'], reply_markup=reply)
        reply.add(KeyboardButton(vocab['cancel']))

        bot.register_next_step_handler_by_chat_id(message.chat.id,
                                                  callback=make_red_step,
                                                  type=type,
                                                  obj=obj,
                                                  added='LINK',
                                                  lang_ad=lang_ad)

    elif message.text == vocab['delete_photo_link']:
        dic = json.loads(open('events.json', encoding='utf-16').read(), encoding='utf-16')
        name = 'events.json'
        db = dbworker.SQLighter(config.db_file)
        lang = db.get_language(message.from_user.id)
        x = list(filter(lambda x: dic[x]['BUTTON'][lang] == obj, dic))[0]

        dic[x]['LINK']['RU'] = ''
        dic[x]['LINK']['KZ'] = ''

        with open(name, 'w', encoding='utf-16') as outfile:
            json.dump(dic, outfile, ensure_ascii=False)
        reply = red_menu(lang, with_photo=True)

        bot.send_message(message.chat.id, vocab['changes_saved'], reply_markup=reply)
        bot.register_next_step_handler_by_chat_id(message.chat.id,
                                                  callback=choose_what_red_step,
                                                  type=type,
                                                  obj=obj)
        db.close()
    elif message.text == vocab['mark_passed']:
        lang_ad = ''
        mess = vocab['input_url_to_photos']
        reply = ReplyKeyboardMarkup(resize_keyboard=True)
        reply.add(KeyboardButton(vocab['without_photos']))
        reply.add(KeyboardButton(vocab['cancel']))
        bot.send_message(message.chat.id, mess, reply_markup=reply)
        bot.register_next_step_handler_by_chat_id(message.chat.id, callback=make_red_step,
                                                  type=type, obj=obj, added='LINK', lang_ad=lang_ad)
    elif message.text == vocab['delete']:
        if type == config.RED_type.EVENT:
            events = json.loads(open('events.json', encoding='utf-16').read(), encoding='utf-16')
            x = list(filter(lambda x: events[x]['BUTTON'][lang] == obj, events))[0]
            z = events.pop(x)
            with open('events.json', 'w', encoding='utf-16') as outfile:
                json.dump(events, outfile, ensure_ascii=False)
        elif type == config.RED_type.PROJECT:
            projects = json.loads(open('projects.json', encoding='utf-16').read(), encoding='utf-16')
            x = list(filter(lambda x: projects[x]['BUTTON'][lang] == obj, projects))[0]
            z = projects.pop(x)
            with open('projects.json', 'w', encoding='utf-16') as outfile:
                json.dump(projects, outfile, ensure_ascii=False)
        elif type == config.RED_type.ORG_ART or type == config.RED_type.ORG_JUN:
            org_type = 'art' if type == config.RED_type.ORG_ART else 'jun'
            orgs = json.loads(open('organizations.json', encoding='utf-16').read(), encoding='utf-16')
            x = list(filter(lambda x: orgs[org_type][x]['BUTTON'][lang] == obj, orgs[org_type]))[0]
            z = orgs[org_type].pop(x)
            with open('organizations.json', 'w', encoding='utf-16') as outfile:
                json.dump(orgs, outfile, ensure_ascii=False)
        bot.send_message(message.chat.id, vocab['changes_saved'], reply_markup=generate_red_info_menu(lang, vocab))
    elif message.text == vocab['chang_but_ru'] or message.text == vocab['chang_but_kz']:
        mess = vocab['input_button_ru'] if message.text == vocab['chang_but_ru'] else vocab['input_button_kz']
        lang_ad = 'RU' if message.text == vocab['chang_but_ru'] else 'KZ'
        reply = ReplyKeyboardMarkup(resize_keyboard=True)
        reply.add(KeyboardButton(vocab['cancel']))
        bot.send_message(message.chat.id, mess, reply_markup=reply)
        bot.register_next_step_handler_by_chat_id(message.chat.id, callback=make_red_step,
                                                  type=type, obj=obj, added='BUTTON', lang_ad=lang_ad)
    elif message.text == vocab['chang_txt_ru'] or message.text == vocab['chang_txt_kz']:
        mess = vocab['input_text_ru'] if message.text == vocab['chang_txt_ru'] else vocab['input_text_kz']
        lang_ad = 'RU' if message.text == vocab['chang_txt_ru'] else 'KZ'
        reply = ReplyKeyboardMarkup(resize_keyboard=True)
        reply.add(KeyboardButton(vocab['cancel']))
        bot.send_message(message.chat.id, mess, reply_markup=reply)
        bot.register_next_step_handler_by_chat_id(message.chat.id, callback=make_red_step,
                                                  type=type, obj=obj, added='TEXT', lang_ad=lang_ad)
    elif message.text == vocab['chang_photo_ru'] or message.text == vocab['chang_photo_kz']:
        mess = vocab['input_photo_ru'] if message.text == vocab['chang_photo_ru'] else vocab['input_photo_kz']
        lang_ad = 'RU' if message.text == vocab['chang_photo_ru'] else 'KZ'
        reply = ReplyKeyboardMarkup(resize_keyboard=True)
        reply.add(KeyboardButton(vocab['cancel']))
        bot.send_message(message.chat.id, mess, reply_markup=reply)
        bot.register_next_step_handler_by_chat_id(message.chat.id, callback=make_red_step,
                                                  type=type, obj=obj, added='PHOTO', lang_ad=lang_ad)
    elif message.text == vocab['chang_file_ru'] or message.text == vocab['chang_file_kz']:
        mess = vocab['input_file_ru'] if message.text == vocab['chang_file_ru'] else vocab['input_file_kz']
        lang_ad = 'RU' if message.text == vocab['chang_file_ru'] else 'KZ'
        reply = ReplyKeyboardMarkup(resize_keyboard=True)
        reply.add(KeyboardButton(vocab['cancel']))
        bot.send_message(message.chat.id, mess, reply_markup=reply)
        bot.register_next_step_handler_by_chat_id(message.chat.id, callback=make_red_step,
                                                  type=type, obj=obj, added='FILE', lang_ad=lang_ad)
    else:
        bot.send_message(message.chat.id, vocab['select'])
        bot.register_next_step_handler_by_chat_id(message.chat.id, callback=choose_what_red_step, type=type,
                                                  obj=obj)
    db.close()


def make_red_step(message, type, obj, added, lang_ad):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')

    if message.text == vocab['fin_change'] or message.text == vocab['cancel']:
        bot.send_message(message.chat.id, vocab['select'], reply_markup=generate_red_info_menu(lang, vocab))
        db.close()
        return
    if type == config.RED_type.PROJECT:
        dic = json.loads(open('projects.json', encoding='utf-16').read(), encoding='utf-16')
        name = 'projects.json'
    elif type == config.RED_type.EVENT:
        dic = json.loads(open('events.json', encoding='utf-16').read(), encoding='utf-16')
        name = 'events.json'
    elif type == config.RED_type.ORG_JUN or type == config.RED_type.ORG_ART:
        dic = json.loads(open('organizations.json', encoding='utf-16').read(), encoding='utf-16')
        name = 'organizations.json'
    if added == 'BUTTON' or added == 'TEXT':
        if message.text:
            if type == config.RED_type.PROJECT or type == config.RED_type.EVENT:
                x = list(filter(lambda x: dic[x]['BUTTON'][lang] == obj, dic))[0]
                dic[x][added][lang_ad] = message.text
            elif type == config.RED_type.ORG_ART:
                x = list(filter(lambda x: dic['art'][x]['BUTTON'][lang] == obj, dic['art']))[0]
                dic['art'][x][added][lang_ad] = message.text
            elif type == config.RED_type.ORG_JUN:
                x = list(filter(lambda x: dic['jun'][x]['BUTTON'][lang] == obj, dic['jun']))[0]
                dic['jun'][x][added][lang_ad] = message.text
            with open(name, 'w', encoding='utf-16') as outfile:
                json.dump(dic, outfile, ensure_ascii=False)
            reply = red_menu(lang, with_photo=True) if type == config.RED_type.EVENT else red_menu(lang, with_file=True)
            bot.send_message(message.chat.id, vocab['changes_saved'], reply_markup=reply)
            bot.register_next_step_handler_by_chat_id(message.chat.id, callback=choose_what_red_step, type=type,
                                                      obj=obj)
            db.close()
        else:
            bot.send_message(message.chat.id, vocab["input_text"])
            bot.register_next_step_handler_by_chat_id(message.chat.id, callback=make_red_step,
                                                      type=type, obj=obj, added=added, lang_ad=lang_ad)
    if added == 'FILE':
        if message.document:
            file_info = bot.get_file(message.document.file_id)
            file = requests.get(
                'https://api.telegram.org/file/bot{0}/{1}'.format(config.token, file_info.file_path))
            with open('info/' + message.document.file_name, 'wb') as outfile:
                outfile.write(file.content)
            if type == config.RED_type.PROJECT:
                x = list(filter(lambda x: dic[x]['BUTTON'][lang] == obj, dic))[0]
                dic[x][added][lang_ad] = message.document.file_id
            elif type == config.RED_type.EVENT:
                x = list(filter(lambda x: dic[x]['BUTTON'][lang] == obj, dic))[0]
                dic[x][added][lang_ad] = message.document.file_id
            elif type == config.RED_type.ORG_ART:
                x = list(filter(lambda x: dic['art'][x]['BUTTON'][lang] == obj, dic['art']))[0]
                dic['art'][x][added][lang_ad] = message.document.file_id
            elif type == config.RED_type.ORG_JUN:
                x = list(filter(lambda x: dic['jun'][x]['BUTTON'][lang] == obj, dic['jun']))[0]
                dic['jun'][x][added][lang_ad] = message.document.file_id
            with open(name, 'w', encoding='utf-16') as outfile:
                json.dump(dic, outfile, ensure_ascii=False)
            reply = red_menu(lang, with_file=True)
            bot.send_message(message.chat.id, vocab['changes_saved'], reply_markup=reply)
            bot.register_next_step_handler_by_chat_id(message.chat.id, callback=choose_what_red_step, type=type,
                                                      obj=obj)
        else:
            bot.send_message(message.chat.id, vocab["input_file"])
            bot.register_next_step_handler_by_chat_id(message.chat.id, callback=make_red_step,
                                                      type=type, obj=obj, added=added, lang_ad=lang_ad)
    if added == 'PHOTO':
        if message.photo:
            x = list(filter(lambda x: dic[x]['BUTTON'][lang] == obj, dic))[0]
            dic[x][added][lang_ad] = message.photo[len(message.photo) - 1].file_id
            with open(name, 'w', encoding='utf-16') as outfile:
                json.dump(dic, outfile, ensure_ascii=False)
            reply = red_menu(lang, with_photo=True)
            bot.send_message(message.chat.id, vocab['changes_saved'], reply_markup=reply)
            bot.register_next_step_handler_by_chat_id(message.chat.id, callback=choose_what_red_step, type=type,
                                                      obj=obj)
        else:
            bot.send_message(message.chat.id, vocab["input_photo"])
            bot.register_next_step_handler_by_chat_id(message.chat.id, callback=make_red_step,
                                                      type=type, obj=obj, added=added, lang_ad=lang_ad)
    if added == 'LINK':
        if message.text:
            x = list(filter(lambda x: dic[x]['BUTTON'][lang] == obj, dic))[0]
            dic[x]['PASSED']['RU'] = 'True'
            dic[x]['PASSED']['KZ'] = 'True'

            if message.text == vocab['without_photos']:
                dic[x][added]['RU'] = ''
                dic[x][added]['KZ'] = ''
            else:
                dic[x][added]['RU'] = message.text
                dic[x][added]['KZ'] = message.text

            with open(name, 'w', encoding='utf-16') as outfile:
                json.dump(dic, outfile, ensure_ascii=False)
            reply = red_menu(lang, with_photo=True)

            bot.send_message(message.chat.id, vocab['changes_saved'], reply_markup=reply)

            bot.register_next_step_handler_by_chat_id(message.chat.id,
                                                      callback=choose_what_red_step,
                                                      type=type,
                                                      obj=obj)
            db.close()
        else:
            bot.send_message(message.chat.id, vocab["input_text"])
            bot.register_next_step_handler_by_chat_id(message.chat.id, callback=make_red_step,
                                                      type=type, obj=obj, added=added, lang_ad=lang_ad)


def red_or_add_step(message, type):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')

    if message.text == vocab['cancel']:
        bot.send_message(message.chat.id, vocab['select'], reply_markup=generate_red_info_menu(lang, vocab))
        db.close()
    elif message.text == vocab['add_new']:
        reply = ReplyKeyboardMarkup(resize_keyboard=True)
        reply.add(vocab['cancel'])
        bot.send_message(message.chat.id, text=vocab['input_button_ru'], reply_markup=reply)
        bot.register_next_step_handler_by_chat_id(message.chat.id, callback=add_data_step, type=type,
                                                  adding='BUTTON_RU', data=[])
        db.close()
    elif message.text == vocab['red_passed_events']:
        events = json.loads(open('events.json', encoding='utf-16').read(), encoding='utf-16')
        reply = ReplyKeyboardMarkup(resize_keyboard=True)
        counter = 0
        for event in events:
            if events[event]["PASSED"][lang] == 'True':
                counter += 1
                reply.add(KeyboardButton(events[event]["BUTTON"][lang]))
        if counter == 0:
            bot.send_message(message.chat.id,
                             text=vocab['no_passed_events'],
                             reply_markup=gen_add_pass_fut_markup(vocab))
            bot.register_next_step_handler_by_chat_id(message.chat.id, callback=red_or_add_step, type=type)
        else:
            reply.add(vocab['cancel'])
            bot.send_message(message.chat.id, text=vocab['select_event'], reply_markup=reply)
            bot.register_next_step_handler_by_chat_id(message.chat.id,
                                                      callback=choose_variant_step,
                                                      type=config.RED_type.PAS_EVENT)
        db.close()
    elif message.text == vocab['red_future_events']:
        events = json.loads(open('events.json', encoding='utf-16').read(), encoding='utf-16')
        reply = ReplyKeyboardMarkup(resize_keyboard=True)
        counter = 0
        for event in events:
            if events[event]["PASSED"][lang] == 'False':
                counter += 1
                reply.add(KeyboardButton(events[event]["BUTTON"][lang]))
        if counter == 0:
            bot.send_message(message.chat.id,
                             text=vocab['no_future_events'],
                             reply_markup=gen_add_pass_fut_markup(vocab))
            bot.register_next_step_handler_by_chat_id(message.chat.id, callback=red_or_add_step, type=type)
        else:
            reply.add(vocab['cancel'])
            bot.send_message(message.chat.id, text=vocab['select_event'], reply_markup=reply)
            bot.register_next_step_handler_by_chat_id(message.chat.id,
                                                      callback=choose_variant_step,
                                                      type=config.RED_type.FUT_EVENT)
        db.close()
    elif message.text == vocab['red_prev']:
        if type == config.RED_type.PROJECT:
            projects = json.loads(open('projects.json', encoding='utf-16').read(), encoding='utf-16')
            reply = ReplyKeyboardMarkup(resize_keyboard=True)
            for proj in projects:
                reply.add(KeyboardButton(projects[proj]["BUTTON"][lang]))
            reply.add(vocab['cancel'])
            bot.send_message(message.chat.id, text=vocab['select_project'], reply_markup=reply)
            bot.register_next_step_handler_by_chat_id(message.chat.id, callback=choose_variant_step, type=type)
            db.close()
        elif type == config.RED_type.ORG_JUN or type == config.RED_type.ORG_ART:
            organizations = json.loads(open('organizations.json', encoding='utf-16').read(), encoding='utf-16')
            reply = ReplyKeyboardMarkup(resize_keyboard=True)
            type_org = 'jun' if type == config.RED_type.ORG_JUN else 'art'
            for org in organizations[type_org]:
                reply.add(KeyboardButton(organizations[type_org][org]["BUTTON"][lang]))
            reply.add(vocab['cancel'])
            bot.send_message(message.chat.id, text=vocab['select_org'], reply_markup=reply)
            bot.register_next_step_handler_by_chat_id(message.chat.id, callback=choose_variant_step, type=type)
            db.close()
    else:
        bot.send_message(message.chat.id, text=vocab['select'], reply_markup=add_red_menu(lang))
        bot.register_next_step_handler_by_chat_id(message.chat.id, callback=red_or_add_step, type=type)


def add_data_step(message, type, adding, data):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')

    if vocab['cancel'] == message.text:
        bot.send_message(message.chat.id, vocab['select'], reply_markup=generate_red_info_menu(lang, vocab))
        db.close()
        return

    if message.text == vocab['without_photo'] or message.text == vocab['without_file']:
        if type == config.RED_type.EVENT:
            if message.text == vocab['without_photo']:
                print(adding)
                if adding == "FILE_RU":
                    data.append('')
                    data.append('')
                    reply = ReplyKeyboardMarkup(resize_keyboard=True)
                    reply.add(vocab['without_file'])
                    reply.add(vocab['cancel'])
                    bot.send_message(message.chat.id, vocab['input_file_ru'], reply_markup=reply)
                    bot.register_next_step_handler_by_chat_id(message.chat.id,
                                                              callback=add_data_step,
                                                              type=type,
                                                              data=data,
                                                              adding="FILE_RU")
                elif adding == "FILE_KZ":
                    data.append('')
                    reply = ReplyKeyboardMarkup(resize_keyboard=True)
                    reply.add(vocab['without_file'])
                    reply.add(vocab['cancel'])
                    bot.send_message(message.chat.id, vocab['input_file_ru'], reply_markup=reply)
                    bot.register_next_step_handler_by_chat_id(message.chat.id,
                                                              callback=add_data_step,
                                                              type=type,
                                                              data=data,
                                                              adding="FILE_RU")
            else:
                print(data)
                save_event(data, with_photo=True, with_files=False)
                bot.send_message(message.chat.id, vocab["changes_saved"],
                                 reply_markup=generate_red_info_menu(lang, vocab))

        elif type == config.RED_type.PROJECT:
            save_project(data, with_files=False)
            bot.send_message(message.chat.id, vocab["changes_saved"], reply_markup=generate_red_info_menu(lang, vocab))

        elif type == config.RED_type.ORG_JUN or type == config.RED_type.ORG_ART:
            org_type = 'jun' if type == config.RED_type.ORG_JUN else 'art'
            save_org(org_type, data, with_files=False)
            bot.send_message(message.chat.id, vocab["changes_saved"], reply_markup=generate_red_info_menu(lang, vocab))

        db.close()
        return

    first_step_list = ['BUTTON_RU', 'BUTTON_KZ', 'TEXT_RU']
    add_list = ['BUTTON_RU', 'BUTTON_KZ', 'TEXT_RU', 'TEXT_KZ']
    phrases = ["input_button_ru", "input_button_kz", "input_text_ru", "input_text_kz"]
    if adding in first_step_list:
        if message.text:
            data.append(message.text)
            bot.send_message(message.chat.id, vocab[phrases[add_list.index(adding) + 1]])
            bot.register_next_step_handler_by_chat_id(message.chat.id,
                                                      callback=add_data_step,
                                                      type=type,
                                                      data=data,
                                                      adding=add_list[first_step_list.index(adding) + 1])
        else:
            bot.send_message(message.chat.id, phrases[first_step_list.index(adding)])
            bot.register_next_step_handler_by_chat_id(message.chat.id, callback=add_data_step, type=type, data=data)
    elif adding == 'TEXT_KZ':
        if message.text:
            data.append(message.text)
            reply = ReplyKeyboardMarkup(resize_keyboard=True)
            if type == config.RED_type.EVENT:
                reply.add(vocab['without_photo'])
                reply.add(vocab['cancel'])
                bot.send_message(message.chat.id, vocab['input_photo_ru'], reply_markup=reply)

                bot.register_next_step_handler_by_chat_id(message.chat.id,
                                                          callback=add_data_step,
                                                          type=type,
                                                          data=data,
                                                          adding='FILE_RU')
            else:
                reply.add(vocab['without_file'])
                reply.add(vocab['cancel'])
                bot.send_message(message.chat.id, vocab['input_file_ru'], reply_markup=reply)
                bot.register_next_step_handler_by_chat_id(message.chat.id,
                                                          callback=add_data_step,
                                                          type=type,
                                                          data=data,
                                                          adding='FILE_RU')
        else:
            bot.send_message(message.chat.id, phrases[first_step_list.index(adding)])
            bot.register_next_step_handler_by_chat_id(message.chat.id, callback=add_data_step, type=type, data=data)
    elif adding == "FILE_RU":
        if message.document and type != config.RED_type.EVENT:
            file_info = bot.get_file(message.document.file_id)
            file = requests.get(
                'https://api.telegram.org/file/bot{0}/{1}'.format(config.token, file_info.file_path))
            file_name = 'info/' + message.document.file_name
            file_id = message.document.file_id
            with open(file_name, 'wb') as outfile:
                outfile.write(file.content)
            data.append(file_id)
            bot.send_message(message.chat.id, vocab['input_file_kz'])
            bot.register_next_step_handler_by_chat_id(message.chat.id, callback=add_data_step, type=type, data=data,
                                                      adding="FILE_KZ")

        elif type == config.RED_type.EVENT and message.photo:
            file_id = message.photo[len(message.photo) - 1].file_id
            data.append(file_id)
            reply = ReplyKeyboardMarkup(resize_keyboard=True)
            reply.add(vocab['without_photo'])
            reply.add(vocab['cancel'])
            bot.send_message(message.chat.id, vocab['input_photo_kz'], reply_markup=reply)
            bot.register_next_step_handler_by_chat_id(message.chat.id,
                                                      callback=add_data_step,
                                                      type=type,
                                                      data=data,
                                                      adding="FILE_KZ")
        elif message.document and type == config.RED_type.EVENT:
            file_info = bot.get_file(message.document.file_id)
            file = requests.get(
                'https://api.telegram.org/file/bot{0}/{1}'.format(config.token, file_info.file_path))
            file_name = 'info/' + message.document.file_name
            file_id = message.document.file_id
            with open(file_name, 'wb') as outfile:
                outfile.write(file.content)
            data.append(file_id)
            reply = ReplyKeyboardMarkup(resize_keyboard=True)
            reply.add(vocab['without_file'])
            reply.add(vocab['cancel'])
            bot.send_message(message.chat.id, vocab['input_file_kz'])
            bot.register_next_step_handler_by_chat_id(message.chat.id, callback=add_data_step, type=type, data=data,
                                                      adding="FILE_KZ")
        else:
            bot.send_message(message.chat.id, phrases[first_step_list.index(adding)])
            bot.register_next_step_handler_by_chat_id(message.chat.id, callback=add_data_step, type=type, data=data)
    elif adding == "FILE_KZ":
        if type == config.RED_type.EVENT and message.photo:
            file_id = message.photo[len(message.photo) - 1].file_id
            data.append(file_id)
            reply = ReplyKeyboardMarkup(resize_keyboard=True)
            reply.add(vocab['without_file'])
            reply.add(vocab['cancel'])
            bot.send_message(message.chat.id, vocab['input_file_ru'], reply_markup=reply)
            bot.register_next_step_handler_by_chat_id(message.chat.id,
                                                      callback=add_data_step,
                                                      type=type,
                                                      data=data,
                                                      adding="FILE_RU")
        elif message.document and type == config.RED_type.EVENT:
            file_info = bot.get_file(message.document.file_id)
            file = requests.get(
                'https://api.telegram.org/file/bot{0}/{1}'.format(config.token, file_info.file_path))
            file_name = 'info/' + message.document.file_name
            file_id = message.document.file_id
            with open(file_name, 'wb') as outfile:
                outfile.write(file.content)
            data.append(file_id)

            save_event(data, with_photo=True, with_files=True)
            bot.send_message(message.chat.id, vocab["changes_saved"], reply_markup=generate_red_info_menu(lang, vocab))

        elif message.document and type != config.RED_type.EVENT:
            if type == config.RED_type.PROJECT:
                file_info = bot.get_file(message.document.file_id)
                file_id = message.document.file_id
                file = requests.get(
                    'https://api.telegram.org/file/bot{0}/{1}'.format(config.token, file_info.file_path))
                file_name = 'info/' + message.document.file_name
                with open(file_name, 'wb') as outfile:
                    outfile.write(file.content)
                data.append(file_id)
                save_project(data, with_files=True)
            elif type == config.RED_type.ORG_JUN or type == config.RED_type.ORG_ART:
                org_type = 'jun' if type == config.RED_type.ORG_JUN else 'art'
                file_info = bot.get_file(message.document.file_id)
                file_id = message.document.file_id
                file = requests.get(
                    'https://api.telegram.org/file/bot{0}/{1}'.format(config.token, file_info.file_path))
                file_name = 'info/' + message.document.file_name
                with open(file_name, 'wb') as outfile:
                    outfile.write(file.content)
                data.append(file_id)
                save_org(org_type, data, with_files=True)
            bot.send_message(message.chat.id, vocab["changes_saved"], reply_markup=generate_red_info_menu(lang, vocab))

        else:
            bot.send_message(message.chat.id, phrases[first_step_list.index(adding)])
            bot.register_next_step_handler_by_chat_id(message.chat.id, callback=add_data_step, type=type, data=data)
    db.close()


def save_event(data, with_photo, with_files):
    events = json.loads(open('events.json', encoding='utf-16').read(), encoding='utf-16')
    counter = json.loads(open('counters.json', encoding='utf-16').read(), encoding='utf-16')
    number = counter['LAST_NUM_EVENT'] + 1
    counter['LAST_NUM_EVENT'] = number
    object = {
        "BUTTON": {
            "RU": data[0],
            "KZ": data[1],
        },
        "TEXT": {
            "RU": data[2],
            "KZ": data[3],
        },
        "PASSED": {
            "RU": "False",
            "KZ": "False",
        },
        "LINK": {
            "RU": "",
            "KZ": "",
        }
    }
    if with_photo:
        object["PHOTO"] = {
            "RU": data[4],
            "KZ": data[5]
        }
    if with_files:
        object["FILE"] = {
            "RU": data[6],
            "KZ": data[7]
        }
    else:
        object["PHOTO"] = {
            "RU": data[4],
            "KZ": data[5]
        }
        object["FILE"] = {
            "RU": "",
            "KZ": ""
        }
    events['event' + str(number)] = object
    with open('events.json', 'w', encoding='utf-16') as outfile:
        json.dump(events, outfile, ensure_ascii=False)
    with open('counters.json', 'w', encoding='utf-16') as outfile:
        json.dump(counter, outfile, ensure_ascii=False)


def save_project(data, with_files):
    projects = json.loads(open('projects.json', encoding='utf-16').read(), encoding='utf-16')
    counter = json.loads(open('counters.json', encoding='utf-16').read(), encoding='utf-16')
    number = counter['LAST_NUM_PROJECT'] + 1
    counter['LAST_NUM_PROJECT'] = number
    object = {
        "BUTTON": {
            "RU": data[0],
            "KZ": data[1],
        },
        "TEXT": {
            "RU": data[2],
            "KZ": data[3],
        }
    }
    if with_files:
        object["FILE"] = {
            "RU": data[4],
            "KZ": data[5]
        }
    else:
        object["FILE"] = {
            "RU": "",
            "KZ": ""
        }
    projects['pr' + str(number)] = object
    with open('projects.json', 'w', encoding='utf-16') as outfile:
        json.dump(projects, outfile, ensure_ascii=False)
    with open('counters.json', 'w', encoding='utf-16') as outfile:
        json.dump(counter, outfile, ensure_ascii=False)


def save_org(type, data, with_files):
    orgs = json.loads(open('organizations.json', encoding='utf-16').read(), encoding='utf-16')
    counter = json.loads(open('counters.json', encoding='utf-16').read(), encoding='utf-16')
    number = counter['LAST_NUM_ORG'][type] + 1
    counter['LAST_NUM_ORG'][type] = number
    obj = {
        "BUTTON": {
            "RU": data[0],
            "KZ": data[1],
        },
        "TEXT": {
            "RU": data[2],
            "KZ": data[3],
        }
    }
    if with_files:
        obj["FILE"] = {
            "RU": data[4],
            "KZ": data[5]
        }
    else:
        obj["FILE"] = {
            "RU": "",
            "KZ": ""
        }
    orgs[type][type + str(number)] = obj
    with open('organizations.json', 'w', encoding='utf-16') as outfile:
        json.dump(orgs, outfile, ensure_ascii=False)
    with open('counters.json', 'w', encoding='utf-16') as outfile:
        json.dump(counter, outfile, ensure_ascii=False)


@bot.message_handler()
def inform_handler(message):
    db = dbworker.SQLighter(config.db_file)
    lang = db.get_language(message.from_user.id)
    info = json.loads(open('info.json', encoding='utf-16').read(), encoding='utf-16')
    vocab = json.loads(open('ru.json', encoding='utf-16').read(), encoding='utf-16') if \
        "RU" == lang else json.loads(open('kz.json', encoding='utf-16').read(), encoding='utf-16')
    delete_prev_inline(message)
    dict_info = {
        vocab['mission']: "mission",
        vocab['rule']: "rule",
        vocab['work']: "work",
        vocab['stud_house']: "stud_house",
        vocab['library']: "library",
        vocab['sport_clubs']: "sport_clubs",
        vocab['army']: "army"
    }

    if message.text in dict_info:
        category = dict_info[message.text]
        if info[category]['File'] == 'True':
            try:
                bot.send_document(chat_id=message.from_user.id, data=info[category][lang])
            except FileNotFoundError:
                bot.send_message(chat_id=message.chat.id, text='FILE NOT FOUND')
        else:
            bot.send_message(chat_id=message.from_user.id, text=info[category][lang], parse_mode='Markdown')
    elif message.text == vocab['back_to_menu']:
        state = db.get_state(message.from_user.id)
        if state == config.States.S_ABOUT_MENU or config.States.S_INFORM_MENU:
            db.set_state(message.from_user.id, config.States.S_MAIN_MENU.value)
            reply_markup = ReplyKeyboardMarkup(resize_keyboard=True)
            # reply_markup.row_width = 1
            reply_markup.row(KeyboardButton(vocab['about']),
                             KeyboardButton(vocab['inform']))
            reply_markup.row(KeyboardButton(vocab['suggest']),
                             KeyboardButton(vocab['events']))
            reply_markup.row(KeyboardButton(vocab['organizations']),
                             KeyboardButton(vocab['change_lang']))
            bot.send_message(chat_id=message.from_user.id, text=vocab['select'], reply_markup=reply_markup)
    else:
        bot.send_message(chat_id=message.from_user.id, text=vocab['unknown_command'])
    db.close()


if __name__ == '__main__':
    while True:
        try:
            bot.polling(none_stop=True)
        except Exception as e:
            print(e)
            time.sleep(15)
