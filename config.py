from enum import Enum

token = "686329184:AAHqEZqnEZVVKK9ZnW0vehdfOEQjL2HcNKA"
debug_token = "643334085:AAFIdlld7k8KHhnVFLHI3Kl10jsh2-f81YI"
db_file = "database.db"


class States(Enum):
    """
    Мы используем БД Vedis, в которой хранимые значения всегда строки,
    поэтому и тут будем использовать тоже строки (str)
    """
    S_START = "0"  # Начало нового диалога
    S_LANGUAGE = "1"
    S_MAIN_MENU = "2"
    S_ABOUT_MENU = "3"
    S_INFORM_MENU = "4"
    S_REG_MENU = "5"
    S_ORG_MENU = "6"
    S_SHOW_EVENT = "7"


class CONV_type(Enum):
    REGISTER_EVENT = "0"
    REGISTER_ORG = "1"
    ADD_SUGGESTION = "2"
    MAKE_APPOINTMENT_TO_PSY = "3"
    MAKE_APPOINTMENT_TO_REL = "4"


class RED_type(Enum):
    PROJECT = "0"
    ORG_JUN = "1"
    ORG_ART = "2"
    EVENT = "3"
    FUT_EVENT = "4"
    PAS_EVENT = "5"


class RED_contact(Enum):
    ADD_NUMBER = "0"
    ADD_SITE = "1"
    DELETE_NUMBER = "2"
    RED_LOC = "3"
    ADDRESS = "4"


def validate_number(number):
    if number[0] == '8':
        return '+7' + number[1:]
    elif number[0] == '7':
        return '+' + number
    return number


