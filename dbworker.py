import sqlite3
import logging
import telebot
import config
from datetime import datetime
from time import strftime
from xlsxwriter.workbook import Workbook


class SQLighter:

    def __init__(self, database):
        self.connection = sqlite3.connect(database)
        self.cursor = self.connection.cursor()
        self.logger = telebot.logger
        self.logger.setLevel(logging.ERROR)

    def add_user(self, user_id, state, lang, number, name):
        try:
            with self.connection:
                self.cursor.execute('INSERT into Users VALUES (?,?,?,?,?)', (user_id, state, lang, number, name))
                self.connection.commit()
        except Exception as e:
            self.logger.error(e)

    def add_suggestion(self, user_name, suggestions, contact, mail, photo=None):
        with self.connection:
            time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            self.cursor.execute('INSERT into SUGGESTIONS VALUES (?,?,?,?,?,?)',
                                (user_name, contact, suggestions, photo, mail, time))
            self.connection.commit()

    def add_psy_appointment(self, user_name, faculty, contact, mail, comment):
        with self.connection:
            time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            self.cursor.execute('INSERT into Psychologist_appointment VALUES (?,?,?,?,?,?)',
                                (user_name, faculty, contact, mail, comment, time))
            self.connection.commit()

    def add_rel_appointment(self, user_name, faculty, contact, mail, comment):
        with self.connection:
            time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            self.cursor.execute('INSERT into Religious_appointment VALUES (?,?,?,?,?,?)',
                                (user_name, faculty, contact, mail, comment, time))
            self.connection.commit()

    def add_admin(self, number):
        with self.connection:
            self.cursor.execute('INSERT into Admins VALUES (?)', [number])
            self.connection.commit()

    def add_registration(self, type, user_name, mail, facult, contact, org):
        with self.connection:
            time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            self.cursor.execute('INSERT into Registrations VALUES (?,?,?,?,?,?,?)',
                                (type, user_name, mail, facult, contact, org, time))
            self.connection.commit()

    # Пытаемся узнать из базы «состояние» пользователя
    def get_state(self, user_id):
        try:
            with self.connection:
                return self.cursor.execute('SELECT STATE FROM Users WHERE ID=?', (user_id,)).fetchone()[0]
        except Exception:
            return config.States.S_MAIN_MENU.value

    # Сохраняем текущее «состояние» пользователя в нашу базу
    def set_state(self, user_id, value):
        with self.connection:
            self.cursor.execute('UPDATE Users SET STATE=? WHERE ID=?', (value, user_id))
            self.connection.commit()

    def get_language(self, user_id):
        try:
            with self.connection:
                return self.cursor.execute('SELECT LANG FROM Users WHERE ID=?', (user_id,)).fetchone()[0]
        except Exception:
            return 'RU'

    def get_number(self, user_id):
        with self.connection:
            return self.cursor.execute('SELECT NUMBER FROM Users WHERE ID=?', (user_id,)).fetchone()[0]

    # Save Language
    def set_language(self, user_id, value):
        with self.connection:
            self.cursor.execute('UPDATE Users SET LANG=? WHERE ID=?', (value, user_id))
            self.connection.commit()

    def is_admin(self, user_id):
        # return True
        number = self.get_number(user_id)
        with self.connection:
            return len(self.cursor.execute('SELECT * FROM Admins WHERE Number=?', (number,)).fetchall()) > 0

    def get_suggestions(self):
        with self.connection:
            output_name = 'suggestions.xlsx'
            workbook = Workbook(output_name)
            worksheet = workbook.add_worksheet()
            mysel = self.cursor.execute('SELECT * FROM SUGGESTIONS')
            counter = 0
            cell_format = workbook.add_format()
            cell_format.set_border()
            header = ['Имя', 'Номер', 'Предложение', 'Фото', 'Почта', 'Время заявки']
            width = [len(i) for i in header]
            worksheet.write_row(0, 0, header, cell_format)
            for i, row in enumerate(mysel):
                counter += 1
                for j in range(len(header)):
                    if row[j] is not None:
                        width[j] = max(width[j], len(row[j]))
                worksheet.write_row(i + 1, 0, row, cell_format)
            width = list(map(lambda x: x * 1.25, width))
            for i, width_cur in enumerate(width):
                worksheet.set_column(i, i, width=width_cur)
            workbook.close()
            return output_name if counter != 0 else 'EMPTY'

    def get_psy_applications(self):
        with self.connection:
            output_name = 'psycology_applications.xlsx'
            workbook = Workbook(output_name)
            worksheet = workbook.add_worksheet()
            mysel = self.cursor.execute('SELECT * FROM Psychologist_appointment')
            counter = 0
            cell_format = workbook.add_format()
            cell_format.set_border()
            header = ['Имя', 'Факультет', 'Номер', 'Почта', 'Комментарий', 'Время заявки']
            width = [len(i) for i in header]
            worksheet.write_row(0, 0, header, cell_format)
            for i, row in enumerate(mysel):
                counter += 1
                for j in range(len(header)):
                    if row[j] is not None:
                        width[j] = max(width[j], len(row[j]))
                worksheet.write_row(i + 1, 0, row, cell_format)
            width = list(map(lambda x: x * 1.25, width))
            for i, width_cur in enumerate(width):
                worksheet.set_column(i, i, width=width_cur)
            workbook.close()
            return output_name if counter != 0 else 'EMPTY'

    def get_rel_applications(self):
        with self.connection:
            output_name = 'religious_applications.xlsx'
            workbook = Workbook(output_name)
            worksheet = workbook.add_worksheet()
            mysel = self.cursor.execute('SELECT * FROM Religious_appointment')
            counter = 0
            cell_format = workbook.add_format()
            cell_format.set_border()
            header = ['Имя', 'Факультет', 'Номер', 'Почта', 'Комментарий', 'Время заявки']
            width = [len(i) for i in header]
            worksheet.write_row(0, 0, header, cell_format)
            for i, row in enumerate(mysel):
                counter += 1
                for j in range(len(header)):
                    if row[j] is not None:
                        width[j] = max(width[j], len(row[j]))
                worksheet.write_row(i + 1, 0, row, cell_format)
            width = list(map(lambda x: x * 1.25, width))
            for i, width_cur in enumerate(width):
                worksheet.set_column(i, i, width=width_cur)
            workbook.close()
            return output_name if counter != 0 else 'EMPTY'

    def get_org_registraitons(self):
        with self.connection:
            output_name = 'org_registrations.xlsx'
            workbook = Workbook(output_name)
            worksheet = workbook.add_worksheet()
            mysel = self.cursor.execute(
                "SELECT NAME,MAIL,FACULT,NUMBER,ORG_NAME,TIME FROM Registrations WHERE TYPE='ORG'")
            counter = 0
            cell_format = workbook.add_format()
            cell_format.set_border()
            header = ['Имя', 'Почта', 'Факультет', 'Номер', 'Организация', 'Время заявки']
            width = [len(i) for i in header]
            worksheet.write_row(0, 0, header, cell_format=cell_format)
            for i, row in enumerate(mysel):
                counter += 1
                for j in range(len(header)):
                    if row[j] is not None:
                        width[j] = max(width[j], len(row[j]))
                worksheet.write_row(i + 1, 0, row, cell_format=cell_format)
            width = list(map(lambda x: x * 1.25, width))
            for i, width_cur in enumerate(width):
                worksheet.set_column(i, i, width=width_cur)
            workbook.close()
            return output_name if counter != 0 else 'EMPTY'

    def get_event_registraitons(self):
        with self.connection:
            output_name = 'event_registrations.xlsx'
            workbook = Workbook(output_name)
            worksheet = workbook.add_worksheet()
            cell_format = workbook.add_format()
            cell_format.set_border()
            mysel = self.cursor.execute(
                "SELECT NAME,MAIL, FACULT, NUMBER, ORG_NAME, TIME FROM Registrations WHERE TYPE='EVENT'")
            counter = 0
            header = ['Имя', 'Почта', 'Факультет', 'Номер', 'Мероприятие', 'Время заявки']
            width = [len(i) for i in header]
            worksheet.write_row(0, 0, header, cell_format=cell_format)
            for i, row in enumerate(mysel):
                counter += 1
                for j in range(len(header)):
                    if row[j] is not None:
                        width[j] = max(width[j], len(row[j]))
                worksheet.write_row(i + 1, 0, row, cell_format=cell_format)
            width = list(map(lambda x: x * 1.25, width))
            for i, width_cur in enumerate(width):
                worksheet.set_column(i, i, width=width_cur)
            workbook.close()
            return output_name if counter != 0 else 'EMPTY'

    def get_admins(self):
        try:
            with self.connection:
                t = self.cursor.execute('SELECT Number FROM Admins').fetchall()
                return list(map(lambda x: x[0], t))
        except Exception:
            return None

    def delete_admin(self, number):
        try:
            with self.connection:
                self.cursor.execute('DELETE FROM Admins WHERE Number=?', (number,))
        except Exception:
            pass

    def get_current_org_registraitons(self, ru_name, kz_name):
        with self.connection:
            output_name = 'org_registrations_' + ru_name + '.xlsx'
            workbook = Workbook(output_name)
            worksheet = workbook.add_worksheet()
            mysel = self.cursor.execute(
                "SELECT NAME,MAIL,FACULT,NUMBER,ORG_NAME,TIME FROM Registrations WHERE TYPE='ORG' AND (ORG_NAME=? OR ORG_NAME=?)",
                (ru_name, kz_name))
            counter = 0
            cell_format = workbook.add_format()
            cell_format.set_border()
            header = ['Имя', 'Почта', 'Факультет', 'Номер', 'Организация', 'Время заявки']
            width = [len(i) for i in header]
            worksheet.write_row(0, 0, header, cell_format=cell_format)
            for i, row in enumerate(mysel):
                counter += 1
                for j in range(len(header)):
                    if row[j] is not None:
                        width[j] = max(width[j], len(row[j]))
                worksheet.write_row(i + 1, 0, row, cell_format=cell_format)
            width = list(map(lambda x: x * 1.25, width))
            for i, width_cur in enumerate(width):
                worksheet.set_column(i, i, width=width_cur)
            workbook.close()
            return output_name if counter != 0 else 'EMPTY'

    def get_current_event_registraitons(self, ru_name, kz_name):
        with self.connection:
            output_name = 'event_registrations_' + ru_name + '.xlsx'
            workbook = Workbook(output_name)
            worksheet = workbook.add_worksheet()
            cell_format = workbook.add_format()
            cell_format.set_border()
            mysel = self.cursor.execute(
                "SELECT NAME,MAIL, FACULT, NUMBER, ORG_NAME, TIME FROM Registrations WHERE TYPE='EVENT' AND (ORG_NAME=? OR ORG_NAME=?)",
                (ru_name, kz_name))
            counter = 0
            header = ['Имя', 'Почта', 'Факультет', 'Номер', 'Мероприятие', 'Время заявки']
            width = [len(i) for i in header]
            worksheet.write_row(0, 0, header, cell_format=cell_format)
            for i, row in enumerate(mysel):
                counter += 1
                for j in range(len(header)):
                    if row[j] is not None:
                        width[j] = max(width[j], len(row[j]))
                worksheet.write_row(i + 1, 0, row, cell_format=cell_format)
            width = list(map(lambda x: x * 1.25, width))
            for i, width_cur in enumerate(width):
                worksheet.set_column(i, i, width=width_cur)
            workbook.close()
            return output_name if counter != 0 else 'EMPTY'

    def close(self):
        """ Закрываем текущее соединение с БД """
        self.connection.close()
